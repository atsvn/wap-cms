use master
GO

IF EXISTS(SELECT name FROM master..sysdatabases WHERE name = N'metfonebox')
	DROP DATABASE metfonebox
GO

create database metfonebox
GO

use metfonebox
GO



---------------------------------------------------------------------------------------------------------
If exists(select name from sysobjects where name='TBL_Account')
	Drop table TBL_Account
Go
Create table TBL_Account
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,Email					nvarchar(255)	not null
	,[Password]				nvarchar(255)	not null
	,Username				nvarchar(255)	not null

	,last_access			datetime

	,Constraint pk_account_id				primary key	(row_id)
	,Constraint ul_account_username			unique		(Username)
)
Go


---------------------------------------------------------------------------------------------------------
If exists(select name from sysobjects where name='TBL_Language')
	Drop table TBL_Language
Go
Create table TBL_Language
(
	 row_id						int identity(1,1) not null
	,language_name				nvarchar(255)	not null
	,language_code				nvarchar(255)	not null
	,language_icon				nvarchar(255)	not null

	,Constraint pk_language_id				primary key	(row_id)
)
Go



---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Game_Category')
Begin
	drop table TBL_Game_Category;
end
Go
create table TBL_Game_Category
(
	  row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,category_name						nvarchar(255)	not null
	

	,constraint pk_game_category_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Tran_Game_Category')
Begin
	drop table TBL_Tran_Game_Category;
end
Go
create table TBL_Tran_Game_Category
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,language_id				int
	,category_row_id			int
	,category_name				nvarchar(255)


	,constraint pk_tran_game_category_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Game')
Begin
	drop table TBL_Game;
end
Go
create table TBL_Game
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,name						nvarchar(255)	not null
	,short_description			nvarchar(2048)
	,[description]				ntext
	,img						nvarchar(255)

	,[view]						int				not null default 0
	,download					int				not null default 0

	,producer					nvarchar(255)

	,androidappurl				nvarchar(255)
	,androidappsize				float

	,javaappurl					nvarchar(255)
	,javaappsize				float

	,iosappurl					nvarchar(255)
	,iosappsize					float

	,constraint pk_game_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Tran_Game')
Begin
	drop table TBL_Tran_Game;
end
Go
create table TBL_Tran_Game
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,language_id				int
	,game_row_id				int
	
	,name						nvarchar(255)
	,short_description			nvarchar(2048)
	,[description]				ntext

	,producer					nvarchar(255)

	,constraint pk_tran_game_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Category_Game')
Begin
	drop table TBL_Category_Game;
end
Go
create table TBL_Category_Game
(
	 row_id						int identity(1,1) not null

	,game_category_row_id		int
	,game_row_id			int


	,constraint pk_category_game_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Video_Category')
Begin
	drop table TBL_Video_Category;
end
Go
create table TBL_Video_Category
(
	  row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,category_name						nvarchar(255)	not null
	

	,constraint pk_video_category_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Tran_Video_Category')
Begin
	drop table TBL_Tran_Video_Category;
end
Go
create table TBL_Tran_Video_Category
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,language_id				int
	,category_row_id			int
	,category_name				nvarchar(255)


	,constraint pk_tran_video_category_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Video')
Begin
	drop table TBL_Video;
end
Go
create table TBL_Video
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,name						nvarchar(255)	not null
	,short_description			nvarchar(2048)
	,[description]				ntext
	,img						nvarchar(255)

	,[view]						int				not null default 0
	,download					int				not null default 0


	,videourl				nvarchar(255)
	,videosize				float


	,constraint pk_video_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Tran_Video')
Begin
	drop table TBL_Tran_Video;
end
Go
create table TBL_Tran_Video
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,language_id				int
	,video_row_id				int
	
	,name						nvarchar(255)
	,short_description			nvarchar(2048)
	,[description]				ntext

	,constraint pk_tran_video_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Category_Video')
Begin
	drop table TBL_Category_Video;
end
Go
create table TBL_Category_Video
(
	 row_id						int identity(1,1) not null

	,video_category_row_id		int
	,video_row_id			int


	,constraint pk_category_video_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Photo_Category')
Begin
	drop table TBL_Photo_Category;
end
Go
create table TBL_Photo_Category
(
	  row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,category_name						nvarchar(255)	not null
	

	,constraint pk_photo_category_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Tran_Photo_Category')
Begin
	drop table TBL_Tran_Photo_Category;
end
Go
create table TBL_Tran_Photo_Category
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,language_id				int
	,category_row_id			int
	,category_name				nvarchar(255)


	,constraint pk_tran_photo_category_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Photo')
Begin
	drop table TBL_Photo;
end
Go
create table TBL_Photo
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,name						nvarchar(255)	not null
	,short_description			nvarchar(2048)

	,img						nvarchar(255)
	,img_ota					nvarchar(255)

	,[view]						int				not null default 0
	,download					int				not null default 0


	,constraint pk_photo_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Tran_Photo')
Begin
	drop table TBL_Tran_Photo;
end
Go
create table TBL_Tran_Photo
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,language_id				int
	,photo_row_id				int
	
	,name						nvarchar(255)
	,short_description			nvarchar(2048)

	,constraint pk_tran_photo_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Category_Photo')
Begin
	drop table TBL_Category_Photo;
end
Go
create table TBL_Category_Photo
(
	 row_id						int identity(1,1) not null

	,photo_category_row_id		int
	,photo_row_id			int


	,constraint pk_category_photo_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Music_Category')
Begin
	drop table TBL_Music_Category;
end
Go
create table TBL_Music_Category
(
	  row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,category_name						nvarchar(255)	not null
	

	,constraint pk_music_category_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Tran_Music_Category')
Begin
	drop table TBL_Tran_Music_Category;
end
Go
create table TBL_Tran_Music_Category
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,language_id				int
	,category_row_id			int
	,category_name				nvarchar(255)


	,constraint pk_tran_music_category_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Music')
Begin
	drop table TBL_Music;
end
Go
create table TBL_Music
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,name						nvarchar(255)	not null
	,short_description			nvarchar(2048)

	,music_file						nvarchar(255)
	,amr_file						nvarchar(255)
	,mmf_file						nvarchar(255)

	,[view]						int				not null default 0
	,download					int				not null default 0


	,constraint pk_music_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Tran_Music')
Begin
	drop table TBL_Tran_Music;
end
Go
create table TBL_Tran_Music
(
	 row_id						int identity(1,1) not null
	,register_person			int				not null
	,register_datetime			datetime		not null
	,update_person				int				not null
	,update_datetime			datetime		not null

	,status						int				not null default 1

	,language_id				int
	,music_row_id				int
	
	,name						nvarchar(255)
	,short_description			nvarchar(2048)

	,constraint pk_tran_music_row_id primary key (row_id)
)
GO


---------------------------------------------------------------------------------------------------------
if exists (select name from sysobjects where name='TBL_Category_Music')
Begin
	drop table TBL_Category_Music;
end
Go
create table TBL_Category_Music
(
	 row_id						int identity(1,1) not null

	,music_category_row_id		int
	,music_row_id			int


	,constraint pk_category_music_row_id primary key (row_id)
)
GO
