﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web.Helpers;

using log4net;

namespace WAP_CMS.Common
{
    public class Util
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        /// <summary>
        /// Create MD5 hash string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GetMD5HashString(string input)
        {
            if (string.IsNullOrEmpty(input))
                return null;

            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }

            return sb.ToString();
        }


        /// <summary>
        /// FTP upload game
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string FtpUploadGame(HttpPostedFileBase file)
        {
            string ftpServer = ConfigurationManager.AppSettings["ftpServer"].ToString() + "/" + ConfigurationManager.AppSettings["ftpGame"].ToString() + "/";
            string username = ConfigurationManager.AppSettings["ftpUsername"].ToString();
            string pwd = ConfigurationManager.AppSettings["ftpPasword"].ToString();


            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + GetFileExtension(file.FileName);
                string ftpAddress = ftpServer + fileName;

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpAddress);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UsePassive = false;
                request.Credentials = new NetworkCredential(username, pwd);

                var sourceStream = file.InputStream;
                Stream requestStream = request.GetRequestStream();
                byte[] buffer = new byte[1024];

                int bytesRead = sourceStream.Read(buffer, 0, 1024);
                do
                {
                    requestStream.Write(buffer, 0, bytesRead);
                    bytesRead = sourceStream.Read(buffer, 0, 1024);
                } while (bytesRead > 0);


                sourceStream.Close();
                requestStream.Close();

                return ConfigurationManager.AppSettings["ftpGame"].ToString() + "/" + fileName;
            }
            catch (Exception ex)
            {
                log.Error("Game file upload", ex);
                return "";
            }

        }

        public static string FtpUploadVideo(HttpPostedFileBase file)
        {
            string ftpServer = ConfigurationManager.AppSettings["ftpServer"].ToString() + "/" + ConfigurationManager.AppSettings["ftpVideo"].ToString() + "/";
            string username = ConfigurationManager.AppSettings["ftpUsername"].ToString();
            string pwd = ConfigurationManager.AppSettings["ftpPasword"].ToString();


            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + GetFileExtension(file.FileName);
                string ftpAddress = ftpServer + fileName;

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpAddress);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UsePassive = false;
                request.Credentials = new NetworkCredential(username, pwd);

                var sourceStream = file.InputStream;
                Stream requestStream = request.GetRequestStream();
                byte[] buffer = new byte[1024];

                int bytesRead = sourceStream.Read(buffer, 0, 1024);
                do
                {
                    requestStream.Write(buffer, 0, bytesRead);
                    bytesRead = sourceStream.Read(buffer, 0, 1024);
                } while (bytesRead > 0);


                sourceStream.Close();
                requestStream.Close();

                return ConfigurationManager.AppSettings["ftpVideo"].ToString() + "/" + fileName;
            }
            catch (Exception ex)
            {
                log.Error("Game file upload", ex);
                return "";
            }

        }

        public static string FtpUploadPhotoOta(HttpPostedFileBase file)
        {
            string ftpServer = ConfigurationManager.AppSettings["ftpServer"].ToString() + "/" + ConfigurationManager.AppSettings["ftpPhotoOta"].ToString() + "/";
            string username = ConfigurationManager.AppSettings["ftpUsername"].ToString();
            string pwd = ConfigurationManager.AppSettings["ftpPasword"].ToString();


            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + GetFileExtension(file.FileName);
                string ftpAddress = ftpServer + fileName;

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpAddress);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UsePassive = false;
                request.Credentials = new NetworkCredential(username, pwd);

                var sourceStream = file.InputStream;
                Stream requestStream = request.GetRequestStream();
                byte[] buffer = new byte[1024];

                int bytesRead = sourceStream.Read(buffer, 0, 1024);
                do
                {
                    requestStream.Write(buffer, 0, bytesRead);
                    bytesRead = sourceStream.Read(buffer, 0, 1024);
                } while (bytesRead > 0);


                sourceStream.Close();
                requestStream.Close();

                return ConfigurationManager.AppSettings["ftpPhotoOta"].ToString() + "/" + fileName;
            }
            catch (Exception ex)
            {
                log.Error("Game file upload", ex);
                return "";
            }

        }


        /// <summary>
        /// FTP upload image
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string FtpUploadGameImg(WebImage file)
        {
            string ftpServer = ConfigurationManager.AppSettings["ftpServer"].ToString() + "/" + ConfigurationManager.AppSettings["ftpGameImg"].ToString() + "/";
            string username = ConfigurationManager.AppSettings["ftpUsername"].ToString();
            string pwd = ConfigurationManager.AppSettings["ftpPasword"].ToString();


            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "." + file.ImageFormat;
                string ftpAddress = ftpServer + fileName;

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpAddress);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UsePassive = false;
                request.Credentials = new NetworkCredential(username, pwd);

                
                Stream requestStream = request.GetRequestStream();
                byte[] buffer = file.GetBytes();

                requestStream.Write(buffer, 0, buffer.Length);

                requestStream.Close();

                return ConfigurationManager.AppSettings["ftpGameImg"].ToString() + "/" + fileName;
            }
            catch (Exception ex)
            {
                log.Error("Game file upload", ex);
                return "";
            }

        }

        public static string FtpUploadVideoImg(WebImage file)
        {
            string ftpServer = ConfigurationManager.AppSettings["ftpServer"].ToString() + "/" + ConfigurationManager.AppSettings["ftpVideoImg"].ToString() + "/";
            string username = ConfigurationManager.AppSettings["ftpUsername"].ToString();
            string pwd = ConfigurationManager.AppSettings["ftpPasword"].ToString();


            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "." + file.ImageFormat;
                string ftpAddress = ftpServer + fileName;

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpAddress);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UsePassive = false;
                request.Credentials = new NetworkCredential(username, pwd);


                Stream requestStream = request.GetRequestStream();
                byte[] buffer = file.GetBytes();

                requestStream.Write(buffer, 0, buffer.Length);

                requestStream.Close();

                return ConfigurationManager.AppSettings["ftpVideoImg"].ToString() + "/" + fileName;
            }
            catch (Exception ex)
            {
                log.Error("Video file upload", ex);
                return "";
            }

        }

        public static string FtpUploadPhotoImg(WebImage file)
        {
            string ftpServer = ConfigurationManager.AppSettings["ftpServer"].ToString() + "/" + ConfigurationManager.AppSettings["ftpPhoto"].ToString() + "/";
            string username = ConfigurationManager.AppSettings["ftpUsername"].ToString();
            string pwd = ConfigurationManager.AppSettings["ftpPasword"].ToString();


            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "." + file.ImageFormat;
                string ftpAddress = ftpServer + fileName;

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpAddress);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UsePassive = false;
                request.Credentials = new NetworkCredential(username, pwd);


                Stream requestStream = request.GetRequestStream();
                byte[] buffer = file.GetBytes();

                requestStream.Write(buffer, 0, buffer.Length);

                requestStream.Close();

                return ConfigurationManager.AppSettings["ftpPhoto"].ToString() + "/" + fileName;
            }
            catch (Exception ex)
            {
                log.Error("Video file upload", ex);
                return "";
            }

        }


        public static string FtpUploadMusic(HttpPostedFileBase file)
        {
            string ftpServer = ConfigurationManager.AppSettings["ftpServer"].ToString() + "/" + ConfigurationManager.AppSettings["ftpMusic"].ToString() + "/";
            string username = ConfigurationManager.AppSettings["ftpUsername"].ToString();
            string pwd = ConfigurationManager.AppSettings["ftpPasword"].ToString();


            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + GetFileExtension(file.FileName);
                string ftpAddress = ftpServer + fileName;

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpAddress);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UsePassive = false;
                request.Credentials = new NetworkCredential(username, pwd);

                var sourceStream = file.InputStream;
                Stream requestStream = request.GetRequestStream();
                byte[] buffer = new byte[1024];

                int bytesRead = sourceStream.Read(buffer, 0, 1024);
                do
                {
                    requestStream.Write(buffer, 0, bytesRead);
                    bytesRead = sourceStream.Read(buffer, 0, 1024);
                } while (bytesRead > 0);


                sourceStream.Close();
                requestStream.Close();

                return ConfigurationManager.AppSettings["ftpMusic"].ToString() + "/" + fileName;
            }
            catch (Exception ex)
            {
                log.Error("Music file upload", ex);
                return "";
            }

        }

        public static string FtpUploadMusicAmr(HttpPostedFileBase file)
        {
            string ftpServer = ConfigurationManager.AppSettings["ftpServer"].ToString() + "/" + ConfigurationManager.AppSettings["ftpMusicAmr"].ToString() + "/";
            string username = ConfigurationManager.AppSettings["ftpUsername"].ToString();
            string pwd = ConfigurationManager.AppSettings["ftpPasword"].ToString();


            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + GetFileExtension(file.FileName);
                string ftpAddress = ftpServer + fileName;

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpAddress);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UsePassive = false;
                request.Credentials = new NetworkCredential(username, pwd);

                var sourceStream = file.InputStream;
                Stream requestStream = request.GetRequestStream();
                byte[] buffer = new byte[1024];

                int bytesRead = sourceStream.Read(buffer, 0, 1024);
                do
                {
                    requestStream.Write(buffer, 0, bytesRead);
                    bytesRead = sourceStream.Read(buffer, 0, 1024);
                } while (bytesRead > 0);


                sourceStream.Close();
                requestStream.Close();

                return ConfigurationManager.AppSettings["ftpMusicAmr"].ToString() + "/" + fileName;
            }
            catch (Exception ex)
            {
                log.Error("MusicAmr file upload", ex);
                return "";
            }

        }

        public static string FtpUploadMusicMmf(HttpPostedFileBase file)
        {
            string ftpServer = ConfigurationManager.AppSettings["ftpServer"].ToString() + "/" + ConfigurationManager.AppSettings["ftpMusicMmf"].ToString() + "/";
            string username = ConfigurationManager.AppSettings["ftpUsername"].ToString();
            string pwd = ConfigurationManager.AppSettings["ftpPasword"].ToString();


            try
            {
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + GetFileExtension(file.FileName);
                string ftpAddress = ftpServer + fileName;

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpAddress);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UsePassive = false;
                request.Credentials = new NetworkCredential(username, pwd);

                var sourceStream = file.InputStream;
                Stream requestStream = request.GetRequestStream();
                byte[] buffer = new byte[1024];

                int bytesRead = sourceStream.Read(buffer, 0, 1024);
                do
                {
                    requestStream.Write(buffer, 0, bytesRead);
                    bytesRead = sourceStream.Read(buffer, 0, 1024);
                } while (bytesRead > 0);


                sourceStream.Close();
                requestStream.Close();

                return ConfigurationManager.AppSettings["ftpMusicMmf"].ToString() + "/" + fileName;
            }
            catch (Exception ex)
            {
                log.Error("MusicMmf file upload", ex);
                return "";
            }

        }

        /// <summary>
        /// Get file extension
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetFileExtension(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return "";

            int index = fileName.LastIndexOf('.');

            if (index > 0)
            {
                return fileName.Substring(index);
            }
            else
                return "";
        }
    }
}