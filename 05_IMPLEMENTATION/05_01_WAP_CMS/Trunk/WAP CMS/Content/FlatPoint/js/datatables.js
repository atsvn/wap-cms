jQuery.noConflict();

jQuery(function($) {

    /* API method to get paging information */
    $.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
        {
        return {
            "iStart":         oSettings._iDisplayStart,
            "iEnd":           oSettings.fnDisplayEnd(),
            "iLength":        oSettings._iDisplayLength,
            "iTotal":         oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage":          oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
            "iTotalPages":    oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
        };
    }

    /* Bootstrap style pagination control */
    $.extend( $.fn.dataTableExt.oPagination, {
        "bootstrap": {
          "fnInit": function( oSettings, nPaging, fnDraw ) {

            var oLang = oSettings.oLanguage.oPaginate;
            console.log(oLang);
            var fnClickHandler = function ( e ) {
              e.preventDefault();
              if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                fnDraw( oSettings );
            }
        };

        $(nPaging).addClass('pagination').append(
          '<ul>'+
          '<li class="prev disabled"><a href="#">Previous</a></li>'+
          '<li class="next disabled"><a href="#">Next</a></li>'+
          '</ul>'
          );
        var els = $('a', nPaging);
        $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
        $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
    },

    "fnUpdate": function ( oSettings, fnDraw ) {
        var iListLength = 5;
        var oPaging = oSettings.oInstance.fnPagingInfo();
        var an = oSettings.aanFeatures.p;
        var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
        if ( oPaging.iTotalPages < iListLength) {
            iStart = 1;
            iEnd = oPaging.iTotalPages;
        }
        else if ( oPaging.iPage <= iHalf ) {
            iStart = 1;
            iEnd = iListLength;
        } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
            iStart = oPaging.iTotalPages - iListLength + 1;
            iEnd = oPaging.iTotalPages;
        } else {
            iStart = oPaging.iPage - iHalf + 1;
            iEnd = iStart + iListLength - 1;
        }
        for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
            // Remove the middle elements
            $('li:gt(0)', an[i]).filter(':not(:last)').remove();

            // Add the new list items and their event handlers
            for ( j=iStart ; j<=iEnd ; j++ ) {
                sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                    .insertBefore( $('li:last', an[i])[0] )
                    .bind('click', function (e) {
                        e.preventDefault();
                        oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                        fnDraw( oSettings );
                });
            }

            // Add / remove disabled classes from the static elements
            if ( oPaging.iPage === 0 ) {
                $('li:first', an[i]).addClass('disabled');
            } else {
                $('li:first', an[i]).removeClass('disabled');
            }
            if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                $('li:last', an[i]).addClass('disabled');
            } else {
                $('li:last', an[i]).removeClass('disabled');
            }
        }
        }
        }
    } );


    //initialize default datatable for all tables that do not has specific id as declared in idList
    var idList = ["photo-index"];
    $('.datatable').each(function(index){
        if ($.inArray(this.id, idList) < 0) {
            $('.datatable').dataTable({
                "sPaginationType": "bootstrap",
                "sDom": "<'tableHeader'<l><'clearfix'f>r>t<'tableFooter'<i><'clearfix'p>>",
                  "iDisplayLength": 10,
                  "aoColumnDefs": [{
                      'bSortable': false,
                      'aTargets': [0]
                  }]
            });

        };
    });

    /////////////////////datatable for Photo/Index//////////////////////
    var photoIndexTable = $('#photo-index').dataTable({
        "sPaginationType": "bootstrap",
        "sDom": "<'tableHeader'<l><'clearfix'f>r>t<'tableFooter'<i><'clearfix'p>>",
        "bServerSide": true,
        "sAjaxSource": "IndexJson",
        "fnDrawCallback": function () {
            $('#iTotalDisplayRecord-photo').empty().append('All record (total: '+this.fnSettings().fnRecordsDisplay()+')');
        },
        "bProcessing": true,
        "aoColumns": [
            {   
                "sName": "ID",
                "bSearchable": false,
                "bSortable": false,
                "fnRender": function (oObj) {
                    return oObj.aData[0];
                }
            },
            { 
                "sName": "NAME",
                "bSearchable": true,
                "bSortable": true,
                "fnRender": function (oObj) {
                    return oObj.aData[1];
                }
            },
            { 
                "sName": "VIEW",
                "bSearchable": false,
                "bSortable": true,
                "fnRender": function (oObj) {
                    return oObj.aData[2];
                }
            },
            { 
                "sName": "DOWNLOAD",
                "bSearchable": false,
                "bSortable": true,
                "fnRender": function (oObj) {
                    return oObj.aData[3];
                }
            },
            { 
                "sName": "ENABLE/DISABLE",
                "bSearchable": false,
                "bSortable": false,
                "fnRender": function (oObj) {
                    if (oObj.aData[4] == 'True') {
                        return '<input class="check-box" disabled="disabled" type="checkbox" checked="checked">';
                    };
                    return '<input class="check-box" disabled="disabled" type="checkbox">';
                }
            },
            { 
                "sName": "EDIT",
                "bSearchable": false,
                "bSortable": false,
                "fnRender": function (oObj) {
                    return '<a href="/Photo/Edit/'+oObj.aData[5]+'" class="grey btn action-btn"><i class="icon-edit"></i> Edit</a>';
                }
            },
            { 
                "sName": "REMOVE",
                "bSearchable": false,
                "bSortable": false,
                "fnRender": function (oObj) {
                    return '<a href="#" onclick="ShowDeletePhotoDlg('+oObj.aData[6]+')" class="btn red delete-btn action-btn" data-toggle="modal"><i class="icon-remove-sign"></i> Delete</a>';
                }
            }
        ]
    });
    $("#photo-index_filter input").unbind().bind("input", function(e) {
        // If the length is 3 or more characters, or the user pressed ENTER, search
        if(this.value.length >= 3 || e.keyCode == 13) {
            photoIndexTable.fnFilter($(this).val());
        }
        if(this.value == "") {
            photoIndexTable.fnFilter("");
        }
        return;
    });
    ///////////////////////////////////////////////////////////////////
    $('.no-search .dataTables_length select').chosen();
});