﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Mail;

using System.Text;
using System.Web.Mvc;

using log4net;
using WAP_CMS.Common;
using WAP_CMS.Models;

namespace WAP_CMS.Controllers
{
    public class AccountController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private CMSDbContext db = new CMSDbContext();

        public ActionResult Signin()
        {
            if (Session["_ADMIN__"] != null)
            {
                return RedirectToAction("Index", "Game");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Signin(TBL_Account accountSignIn)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string hashPassword = Util.GetMD5HashString(accountSignIn.password);
                    TBL_Account account = db.TBL_Accounts.Single(a => a.username.Equals(accountSignIn.username) && a.password.Equals(hashPassword) && a.status == 1);

                    account.last_access = DateTime.Now;
                    db.Entry(account).State = EntityState.Modified;
                    db.SaveChanges();

                    Session["_ADMIN__"] = account;

                    return RedirectToAction("Index", "Game");
                }
                catch (InvalidOperationException ex)
                {
                    ModelState.AddModelError("Password", "Invalid username or password");
                    return View(accountSignIn);
                }
                catch (Exception exception)
                {
                    log.Error("Account/Signin", exception);

                    ModelState.AddModelError("Password", "Sign in failed. Please try again later.");
                    
                    return View(accountSignIn);
                }
            }

            return View(accountSignIn);
        }


        public ActionResult Signout()
        {
            Session.Clear();
            return RedirectToAction("Signin");
        }


        [AccountAuthorization]
        public ActionResult ChangePassword() {
            return View();
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel changePassword)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    bool isError = false;

                    TBL_Account account = db.TBL_Accounts.Find(((TBL_Account)Session["_ADMIN__"]).row_id);

                    if (!account.password.Equals(Util.GetMD5HashString(changePassword.CurrentPassword)))
                    {
                        ModelState.AddModelError("CurrentPassword", "The Current password field is incorrect.");
                        isError = true;
                    }

                    if (isError) {
                        return View(changePassword);
                    }

                    account.update_datetime = DateTime.Now;
                    account.password = Util.GetMD5HashString(changePassword.NewPassword);

                    db.Entry(account).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index", "Game");
                }
                catch (Exception exception)
                {
                    log.Error("Account/ChangePassword", exception);
                    ModelState.AddModelError("ConfirmPassword", "Reset password failed. Please try again later.");

                    return View(changePassword);
                }
            }

            return View(changePassword);
        }
    }
}