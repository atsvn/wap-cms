﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using log4net;
using WAP_CMS.Models;
using System.Web.Script.Serialization;

namespace WAP_CMS.Controllers
{
    public class GameCategoryController : Controller
    {
        private CMSDbContext db = new CMSDbContext();

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [AccountAuthorization]
        public ActionResult Index()
        {
            try
            {
                //var categories = db.TBL_Game_Categorys.Where(c => c.status != 9).OrderBy(c => c.category_name);
                using (db)
                {
                    var categories = from cat in db.TBL_Game_Categorys
                                     where cat.status != 9
                                     select new
                                     {
                                         cat,
                                         total = (from r in db.TBL_Category_Games where r.game_category_row_id == cat.row_id select r).Count()
                                     }; 

                    List<TBL_Game_Category> lst = new List<TBL_Game_Category>();

                    foreach (var category in categories)
                    {
                        category.cat.totalGames = category.total;

                        lst.Add(category.cat);
                    }

                    return View("Index", lst);
                }

            }
            catch (Exception exception)
            {
                log.Error("Game/Category", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        public ActionResult Create()
        {
            return View();
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult Create(TBL_Game_Category category)
        {            
            try
            {
                TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);

                category.register_person = registerPerson.row_id;
                category.update_person = registerPerson.row_id;
                category.register_datetime = DateTime.Now;
                category.update_datetime = DateTime.Now;

                if (ModelState.IsValid)
                {
                    int checkGame = db.TBL_Game_Categorys.Where(c => c.category_name.Equals(category.category_name)).Count();

                    if (checkGame > 0)
                    {
                        ModelState.AddModelError("category_name", "Category name is already exists! Please choose another");

                        return View(category);
                    }
                    else
                    {
                        db.TBL_Game_Categorys.Add(category);
                        db.SaveChanges();

                        TempData["MESSAGE"] = "Category is created successfully";
                        return RedirectToAction("Edit", new { id = category.row_id});
                    }
                }

                return View(category);
            }
            catch (Exception exception)
            {
                log.Error("Game Category/Create", exception);
                return View("Error");
            }

        }


        [AccountAuthorization]
        public ActionResult Edit(int id = 0)
        {
            try
            {
                ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                TBL_Game_Category category = db.TBL_Game_Categorys.Find(id);

                if (category != null)
                {
                    using (db)
                    {
                        var gameCategoriesTran = from cat in db.TBL_Tran_Game_Categorys
                                                 join lang in db.TBL_Languages
                                                 on cat.language_id equals lang.row_id
                                                 where cat.category_row_id == category.row_id
                                                 select new
                                                 {
                                                     cat
                                                     , lang.language_name
                                                     
                                                 };

                        List<TBL_Tran_Game_Category> gameCats = new List<TBL_Tran_Game_Category>();

                        foreach (var gameCat in gameCategoriesTran)
                        {
                            gameCat.cat.language_name = gameCat.language_name;

                            gameCats.Add(gameCat.cat);
                        }


                        ViewBag.TranGameCategories = gameCats;
                    }

                    return View(category);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Game Category/Edit/id", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult Edit(TBL_Game_Category category)
        {
            try
            {
                using (db)
                {
                    ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                    var gameCategoriesTran = from cat in db.TBL_Tran_Game_Categorys
                                             join lang in db.TBL_Languages
                                             on cat.language_id equals lang.row_id
                                             where cat.category_row_id == category.row_id
                                             select new
                                             {
                                                 cat
                                                 ,
                                                 lang.language_name

                                             };

                    List<TBL_Tran_Game_Category> gameCats = new List<TBL_Tran_Game_Category>();

                    foreach (var gameCat in gameCategoriesTran)
                    {
                        gameCat.cat.language_name = gameCat.language_name;

                        gameCats.Add(gameCat.cat);
                    }


                    ViewBag.TranGameCategories = gameCats;
                

                    TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);

                    category.update_person = registerPerson.row_id;
                    category.update_datetime = DateTime.Now;

                    if (ModelState.IsValid)
                    {
                        int checkGame = db.TBL_Game_Categorys.Where(c => c.category_name.Equals(category.category_name) && c.row_id != category.row_id).Count();

                        if (checkGame > 0)
                        {
                            ModelState.AddModelError("category_name", "Category name is already exists! Please choose another");

                            return View(category);
                        }
                        else
                        {
                            db.Entry(category).State = EntityState.Modified;
                            db.SaveChanges();

                            TempData["MESSAGE"] = "Category is saved successfully";
                            return View(category);
                        }
                    }
                }

                return View(category);
            }
            catch (Exception exception)
            {
                log.Error("Game Category/Edit", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpGet]
        public ActionResult TranGameCategory(int catid, int lang = 0)
        {
            try
            {
                if (catid > 0 && lang > 0)
                {
                    TBL_Tran_Game_Category tranGameCategory = db.TBL_Tran_Game_Categorys.Where(g => g.language_id == lang && g.category_row_id == catid).SingleOrDefault();

                    if (tranGameCategory != null)
                    {
                        return PartialView("TranGameCategory", tranGameCategory);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("Tran Game Category", exception);
                return View("Error");
            }
            return PartialView();
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult TranGameCategory(TBL_Tran_Game_Category tranGameCategory)
        {
            try
            {
                TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);


                tranGameCategory.update_person = registerPerson.row_id;

                tranGameCategory.update_datetime = DateTime.Now;

                if (tranGameCategory.row_id == 0)
                {
                    tranGameCategory.register_person = registerPerson.row_id;
                    tranGameCategory.register_datetime = DateTime.Now;
                }

                //if (ModelState.IsValid)
                //{

                    TBL_Tran_Game_Category existsTran = db.TBL_Tran_Game_Categorys.Where(t => t.language_id == tranGameCategory.language_id && t.category_row_id == tranGameCategory.category_row_id).SingleOrDefault();

                    if (existsTran != null)
                    {
                        existsTran.update_person = tranGameCategory.update_person;
                        existsTran.update_datetime = tranGameCategory.update_datetime;
                        existsTran.category_name = tranGameCategory.category_name;
                        existsTran.status = tranGameCategory.status;                        

                        db.Entry(existsTran).State = EntityState.Modified;

                        db.SaveChanges();
                    }
                    else
                    {
                        db.TBL_Tran_Game_Categorys.Add(tranGameCategory);
                        db.SaveChanges();
                    }

                    using (db)
                    {
                        var gameCategoriesTran = from cat in db.TBL_Tran_Game_Categorys
                                                  join lang in db.TBL_Languages
                                                  on cat.language_id equals lang.row_id
                                                  where cat.category_row_id == tranGameCategory.category_row_id
                                                  select new {
                                                              cat.row_id
                                                            , lang.language_name
                                                            , cat.language_id
                                                            , cat.category_name
                                                            , cat.status
                                                  };




                        return this.Json(gameCategoriesTran.ToList());
                    }
                    
                //}
            }
            catch (Exception exception)
            {
                log.Error("Tran Game Category", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult DeleteTranGameCategory(int catid, int langId = 0)
        {
            try
            {
                
                using (db)
                {
                    List<TBL_Tran_Game_Category> categories = (from cat in db.TBL_Tran_Game_Categorys
                                                                where cat.category_row_id == catid && cat.language_id == langId
                                                                select cat).ToList();

                    foreach (TBL_Tran_Game_Category tranGameCat in categories)
                        db.TBL_Tran_Game_Categorys.Remove(tranGameCat);

                    db.SaveChanges();
                    

                    var gameCategoriesTran = from cat in db.TBL_Tran_Game_Categorys
                                             join lang in db.TBL_Languages
                                             on cat.language_id equals lang.row_id
                                             where cat.category_row_id == catid
                                             select new
                                             {
                                                 cat.row_id
                                                 ,
                                                 lang.language_name
                                                 ,
                                                 cat.language_id
                                                 ,
                                                 cat.category_name
                                                 ,
                                                 cat.status
                                             };




                    return this.Json(gameCategoriesTran.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Tran Game Category", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        public ActionResult Detail(int id = 0)
        {
            try
            {
                ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                TBL_Game_Category category = db.TBL_Game_Categorys.Find(id);

                if (category != null)
                {
                    using (db)
                    {
                        var gameCategoriesTran = from cat in db.TBL_Tran_Game_Categorys
                                                 join lang in db.TBL_Languages
                                                 on cat.language_id equals lang.row_id
                                                 where cat.category_row_id == category.row_id
                                                 select new
                                                 {
                                                     cat
                                                     ,
                                                     lang.language_name

                                                 };

                        List<TBL_Tran_Game_Category> gameCats = new List<TBL_Tran_Game_Category>();

                        foreach (var gameCat in gameCategoriesTran)
                        {
                            gameCat.cat.language_name = gameCat.language_name;

                            gameCats.Add(gameCat.cat);
                        }


                        ViewBag.TranGameCategories = gameCats;
                    }

                    return View(category);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Game Category/Detail/id", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                // Check if exists game in category
                int checkExists = db.TBL_Category_Games.Where(c => c.game_category_row_id == id).Count();

                if (checkExists > 0)
                {
                    TBL_Game_Category gameCategory = db.TBL_Game_Categorys.Where(g => g.row_id == id).SingleOrDefault();

                    TBL_Account updatePerson = ((TBL_Account)Session["_ADMIN__"]);

                    if (gameCategory != null)
                    {
                        gameCategory.update_person = updatePerson.row_id;
                        gameCategory.update_datetime = DateTime.Now;

                        gameCategory.status = 9;

                        db.Entry(gameCategory).State = EntityState.Modified;

                        db.SaveChanges();
                    }
                }
                else
                {
                    // Physical remove from db
                    // First remove game translate
                    using (db)
                    {
                        var tranGameCategories = from t in db.TBL_Tran_Game_Categorys
                                                 where t.category_row_id == id
                                                    select t;

                        foreach (TBL_Tran_Game_Category tranCat in tranGameCategories)
                            db.Entry(tranCat).State = EntityState.Deleted;

                        db.SaveChanges();


                        var gameCategories = from t in db.TBL_Game_Categorys
                                      where t.row_id == id
                                      select t;

                        foreach (TBL_Game_Category gameCat in gameCategories)
                            db.Entry(gameCat).State = EntityState.Deleted;

                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Game Category/Delete/id", exception);
                return View("Error");
            }
        }
    }
}