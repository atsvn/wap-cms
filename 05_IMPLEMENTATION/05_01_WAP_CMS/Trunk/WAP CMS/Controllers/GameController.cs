﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using log4net;
using WAP_CMS.Models;
using WAP_CMS.Common;


namespace WAP_CMS.Controllers
{
    public class GameController : Controller
    {
        private CMSDbContext db = new CMSDbContext();

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Dictionary<string, HttpPostedFileBase> postedFileList = new Dictionary<string, HttpPostedFileBase>();

        [AccountAuthorization]
        public ActionResult Index()
        {
            try
            {
                var categories = db.TBL_Game_Categorys.Where(c => c.status != 9);

                ViewBag.GameCategory = new SelectList(categories.ToList(), "row_id", "category_name");

                var games = db.TBL_Games.Where(g => g.status != 9);
                return View("Index", games.ToList());
            }
            catch (Exception exception)
            {
                log.Error("Game/Index", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        public ActionResult Create() {
            return View();
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Create(TBL_Game game)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    bool isError = false;
                    HttpFileCollectionBase postedFiles = Request.Files;

                    for (int i = 0; i < postedFiles.Count; i++)
                    {
                        HttpPostedFileBase file = postedFiles[i];
                        string fileName = file.FileName.ToLower();


                        switch (postedFiles.GetKey(i))
                        {
                            case "Image":
                                if (fileName.Length > 0)
                                {
                                    if (file.ContentType.Contains("image"))
                                    {
                                        postedFileList.Add("Image", file);
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("ImageURL", "The Image field must be image.");
                                        isError = true;
                                    }
                                }
                                else
                                {
                                    postedFileList.Remove("Image");
                                }

                                break;

                            case "AndroidApp":
                                if (fileName.Length > 0)
                                {
                                    if ((file.ContentType.Contains("application/octet-stream") || file.ContentType.Contains("application/vnd.android.package-archive")) && fileName.Contains(".apk"))
                                    {
                                        postedFileList.Add("AndroidApp", file);
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("androidappurl", "File must be Android Application.");
                                        isError = true;
                                    }
                                }
                                else
                                {
                                    postedFileList.Remove("AndroidApp");
                                }

                                break;

                            case "iOSApp":
                                if (fileName.Length > 0)
                                {
                                    if (file.ContentType.Contains("application/x-itunes-ipa") && fileName.Contains(".ipa"))
                                    {
                                        postedFileList.Add("iOSApp", file);
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("iosappurl", "File must be iOS Application.");
                                        isError = true;
                                    }
                                }
                                else
                                {
                                    postedFileList.Remove("iOSApp");
                                }

                                break;

                            case "JavaApp":
                                if (fileName.Length > 0)
                                {
                                    if (file.ContentType.Contains("application/octet-stream") && fileName.Contains(".jad"))
                                    {
                                        postedFileList.Add("JavaApp", file);
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("javaappurl", "File must be Java Application.");
                                        isError = true;
                                    }
                                }
                                else
                                {
                                    postedFileList.Remove("JavaApp");
                                }

                                break;

                            default:
                                break;
                        }
                    }

                    // File upload error
                    if (isError)
                    {
                        return View(game);
                    }
                    else
                    {
                        foreach (var item in postedFileList)
                        {
                            HttpPostedFileBase file = item.Value;

                            switch (item.Key)
                            {
                                case "Image":
                                    game.img = Util.FtpUploadGameImg((new WebImage(file.InputStream)).Resize(Convert.ToInt32(ConfigurationManager.AppSettings["ImageWidth"]), Convert.ToInt32(ConfigurationManager.AppSettings["ImageHeight"]), true, true));

                                    break;

                                case "AndroidApp":
                                    game.androidappsize = file.ContentLength;
                                    game.androidappurl = Util.FtpUploadGame(file);

                                    break;

                                case "iOSApp":
                                    game.iosappsize = file.ContentLength;
                                    game.iosappurl = Util.FtpUploadGame(file);
                                    

                                    break;

                                case "JavaApp":
                                    game.javaappsize = file.ContentLength;
                                    game.javaappurl = Util.FtpUploadGame(file);
                                    

                                    break;
                            }
                        }

                        TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);

                        game.register_person = registerPerson.row_id;
                        game.update_person = registerPerson.row_id;

                        game.register_datetime = DateTime.Now;
                        game.update_datetime = DateTime.Now;

                        db.TBL_Games.Add(game);
                        db.SaveChanges();

                        TempData["MESSAGE"] = "Game is created successfully";
                        return RedirectToAction("Edit", new { id = game.row_id });
                    }

                    
                }
                catch (Exception exception)
                {
                    log.Error("Game/Create", exception);
                    return View("Error");
                }
            }

            return View(game);
        }

        [AccountAuthorization]
        public ActionResult Edit(int id = 0)
        {
            try
            {
                TBL_Game game = db.TBL_Games.Find(id);

                if (game != null)
                {
                    ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                    using (db)
                    {
                        var gameTrans = from g in db.TBL_Tran_Games
                                                 join lang in db.TBL_Languages
                                                 on g.language_id equals lang.row_id
                                                 where g.game_row_id == game.row_id
                                                 select new
                                                 {
                                                     g
                                                     ,
                                                     lang.language_name

                                                 };

                        List<TBL_Tran_Game> games = new List<TBL_Tran_Game>();

                        foreach (var gameTran in gameTrans)
                        {
                            gameTran.g.language_name = gameTran.language_name;

                            games.Add(gameTran.g);
                        }


                        ViewBag.TranGames = games;


                        game.allCategories = db.TBL_Game_Categorys.Where(c => c.status != 9).ToList();

                        var gameCategorys = from c in db.TBL_Game_Categorys
                                        join l in db.TBL_Category_Games
                                        on c.row_id equals l.game_category_row_id
                                        where l.game_row_id == game.row_id && c.status != 9
                                        select new
                                        {
                                            c
                                        };

                        game.categories = new List<TBL_Game_Category>();
                        foreach (var category in gameCategorys)
                        {
                            game.categories.Add(category.c);
                        }
                    }

                    return View(game);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Game/Edit/id", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Edit(TBL_Game game)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (db)
                    {
                        ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                        var gameTrans = from g in db.TBL_Tran_Games
                                        join lang in db.TBL_Languages
                                        on g.language_id equals lang.row_id
                                        where g.game_row_id == game.row_id
                                        select new
                                        {
                                            g
                                            ,lang.language_name

                                        };

                        List<TBL_Tran_Game> games = new List<TBL_Tran_Game>();

                        foreach (var gameTran in gameTrans)
                        {
                            gameTran.g.language_name = gameTran.language_name;

                            games.Add(gameTran.g);
                        }


                        ViewBag.TranGames = games;
                    

                        bool isError = false;
                        HttpFileCollectionBase postedFiles = Request.Files;

                        for (int i = 0; i < postedFiles.Count; i++)
                        {
                            HttpPostedFileBase file = postedFiles[i];
                            string fileName = file.FileName.ToLower();


                            switch (postedFiles.GetKey(i))
                            {
                                case "Image":
                                    if (fileName.Length > 0)
                                    {
                                        if (file.ContentType.Contains("image"))
                                        {
                                            postedFileList.Add("Image", file);
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("ImageURL", "The Image field must be image.");
                                            isError = true;
                                        }
                                    }
                                    else
                                    {
                                        postedFileList.Remove("Image");
                                    }

                                    break;

                                case "AndroidApp":
                                    if (fileName.Length > 0)
                                    {
                                        if ((file.ContentType.Contains("application/octet-stream") || file.ContentType.Contains("application/vnd.android.package-archive")) && fileName.Contains(".apk"))
                                        {
                                            postedFileList.Add("AndroidApp", file);
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("androidappurl", "File must be Android Application.");
                                            isError = true;
                                        }
                                    }
                                    else
                                    {
                                        postedFileList.Remove("AndroidApp");
                                    }

                                    break;

                                case "iOSApp":
                                    if (fileName.Length > 0)
                                    {
                                        if (file.ContentType.Contains("application/x-itunes-ipa") && fileName.Contains(".ipa"))
                                        {
                                            postedFileList.Add("iOSApp", file);
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("iosappurl", "File must be iOS Application.");
                                            isError = true;
                                        }
                                    }
                                    else
                                    {
                                        postedFileList.Remove("iOSApp");
                                    }

                                    break;

                                case "JavaApp":
                                    if (fileName.Length > 0)
                                    {
                                        if ((file.ContentType.Contains("application/octet-stream") || file.ContentType.Contains("application/java-archive")) && (fileName.Contains(".jad") || fileName.Contains(".jar")))
                                        {
                                            postedFileList.Add("JavaApp", file);
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("javaappurl", "File must be Java Application.");
                                            isError = true;
                                        }
                                    }
                                    else
                                    {
                                        postedFileList.Remove("JavaApp");
                                    }

                                    break;

                                default:
                                    break;
                            }
                        }

                        // File upload error
                        if (isError)
                        {
                            game.allCategories = db.TBL_Game_Categorys.Where(c => c.status != 9).ToList();

                            var gameCategorys = from c in db.TBL_Game_Categorys
                                                join l in db.TBL_Category_Games
                                                on c.row_id equals l.game_category_row_id
                                                where l.game_row_id == game.row_id && c.status != 9
                                                select new
                                                {
                                                    c
                                                };

                            game.categories = new List<TBL_Game_Category>();
                            foreach (var category in gameCategorys)
                            {
                                game.categories.Add(category.c);
                            }

                            return View(game);
                        }
                        else
                        {
                            foreach (var item in postedFileList)
                            {
                                HttpPostedFileBase file = item.Value;

                                switch (item.Key)
                                {
                                    case "Image":
                                        game.img = Util.FtpUploadGameImg((new WebImage(file.InputStream)).Resize(Convert.ToInt32(ConfigurationManager.AppSettings["ImageWidth"]), Convert.ToInt32(ConfigurationManager.AppSettings["ImageHeight"]), true, true));

                                        break;

                                    case "AndroidApp":
                                        game.androidappsize = file.ContentLength;
                                        game.androidappurl = Util.FtpUploadGame(file);

                                        break;

                                    case "iOSApp":
                                        game.iosappsize = file.ContentLength;
                                        game.iosappurl = Util.FtpUploadGame(file);


                                        break;

                                    case "JavaApp":
                                        game.javaappsize = file.ContentLength;
                                        game.javaappurl = Util.FtpUploadGame(file);


                                        break;
                                }
                            }

                            TBL_Account updatePerson = ((TBL_Account)Session["_ADMIN__"]);

                            game.update_person = updatePerson.row_id;
                            game.update_datetime = DateTime.Now;

                            db.Entry(game).State = EntityState.Modified;
                            db.SaveChanges();

                            // Remove old game category
                            List<TBL_Category_Game> categoryGames = (from c in db.TBL_Category_Games
                                                                     where c.game_row_id == game.row_id
                                                                       select c).ToList();

                            if (categoryGames.Count > 0)
                            {
                                foreach (TBL_Category_Game ob in categoryGames)
                                    db.TBL_Category_Games.Remove(ob);

                                db.SaveChanges();
                            }

                            // Add news
                            if (game.PostedCategoryRowIds != null && game.PostedCategoryRowIds.Length > 0)
                            {
                                for (int i = 0; i < game.PostedCategoryRowIds.Length; i++)
                                {
                                    TBL_Category_Game cg = new TBL_Category_Game();
                                    cg.game_row_id = game.row_id;
                                    cg.game_category_row_id = Convert.ToInt32(game.PostedCategoryRowIds[i]);

                                    db.TBL_Category_Games.Add(cg);
                                }
                                db.SaveChanges();
                            }


                            game.allCategories = db.TBL_Game_Categorys.Where(c => c.status != 9).ToList();

                            var gameCategorys = from c in db.TBL_Game_Categorys
                                                join l in db.TBL_Category_Games
                                                on c.row_id equals l.game_category_row_id
                                                where l.game_row_id == game.row_id && c.status != 9
                                                select new
                                                {
                                                    c
                                                };

                            game.categories = new List<TBL_Game_Category>();
                            foreach (var category in gameCategorys)
                            {
                                game.categories.Add(category.c);
                            }


                            TempData["MESSAGE"] = "Game is saved successfully";
                            return View(game);
                        }
                    }
                }
                catch (Exception exception)
                {
                    log.Error("Game/Edit", exception);
                    return View("Error");
                }
            }

            return View(game);
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                TBL_Account deletePerson = ((TBL_Account)Session["_ADMIN__"]);

                TBL_Game game = db.TBL_Games.Find(id);

                if (game != null)
                {
                    game.update_person = deletePerson.row_id;
                    game.update_datetime = DateTime.Now;

                    game.status = 9;

                    db.Entry(game).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Game/Delete", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpGet]
        public ActionResult TranGame(int gameid, int lang = 0)
        {
            try
            {
                if (gameid > 0 && lang > 0)
                {
                    TBL_Tran_Game tranGame = db.TBL_Tran_Games.Where(g => g.language_id == lang && g.game_row_id == gameid).SingleOrDefault();

                    if (tranGame != null)
                    {
                        return PartialView("TranGame", tranGame);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("Tran Game", exception);
                return View("Error");
            }
            return PartialView();
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult TranGame(TBL_Tran_Game tranGame)
        {
            try
            {
                TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);


                tranGame.update_person = registerPerson.row_id;

                tranGame.update_datetime = DateTime.Now;

                if (tranGame.row_id == 0)
                {
                    tranGame.register_person = registerPerson.row_id;
                    tranGame.register_datetime = DateTime.Now;
                }

                //if (ModelState.IsValid)
                //{

                TBL_Tran_Game existsTran = db.TBL_Tran_Games.Where(t => t.language_id == tranGame.language_id && t.game_row_id == tranGame.game_row_id).SingleOrDefault();

                if (existsTran != null)
                {
                    existsTran.update_person = tranGame.update_person;
                    existsTran.update_datetime = tranGame.update_datetime;

                    existsTran.name = tranGame.name;
                    existsTran.producer = tranGame.producer;
                    existsTran.short_description = tranGame.short_description;
                    existsTran.description = tranGame.description;

                    existsTran.status = tranGame.status;

                    db.Entry(existsTran).State = EntityState.Modified;

                    db.SaveChanges();
                }
                else
                {
                    db.TBL_Tran_Games.Add(tranGame);
                    db.SaveChanges();
                }

                using (db)
                {
                    var games = from g in db.TBL_Tran_Games
                                             join lang in db.TBL_Languages
                                             on g.language_id equals lang.row_id
                                                where g.game_row_id == tranGame.game_row_id
                                             select new
                                             {
                                                 g.row_id
                                                 ,
                                                 lang.language_name
                                                 ,
                                                 g.language_id
                                                 ,
                                                 g.name
                                                 ,
                                                 g.status
                                             };




                    return this.Json(games.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Tran Game", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult DeleteTranGame(int gameid, int langId = 0)
        {
            try
            {

                using (db)
                {
                    List<TBL_Tran_Game> tranGames = (from g in db.TBL_Tran_Games
                                                     where g.game_row_id == gameid && g.language_id == langId
                                                               select g).ToList();

                    foreach (TBL_Tran_Game tranGame in tranGames)
                        db.TBL_Tran_Games.Remove(tranGame);

                    db.SaveChanges();


                    var games = from g in db.TBL_Tran_Games
                                join lang in db.TBL_Languages
                                on g.language_id equals lang.row_id
                                where g.game_row_id == gameid
                                select new
                                {
                                    g.row_id
                                    ,
                                    lang.language_name
                                    ,
                                    g.language_id
                                    ,
                                    g.name
                                    ,
                                    g.status
                                };




                    return this.Json(games.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Delete Tran Game", exception);
                return View("Error");
            }
        }


    }
}