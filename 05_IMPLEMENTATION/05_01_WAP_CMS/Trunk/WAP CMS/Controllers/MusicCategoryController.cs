﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using log4net;
using WAP_CMS.Models;
using System.Web.Script.Serialization;

namespace WAP_CMS.Controllers
{
    public class MusicCategoryController : Controller
    {
        private CMSDbContext db = new CMSDbContext();

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [AccountAuthorization]
        public ActionResult Index()
        {
            try
            {
                //var categories = db.TBL_Music_Categorys.Where(c => c.status != 9).OrderBy(c => c.category_name);
                using (db)
                {
                    var categories = from cat in db.TBL_Music_Categorys
                                     where cat.status != 9
                                     select new
                                     {
                                         cat,
                                         total = (from r in db.TBL_Category_Musics where r.music_category_row_id == cat.row_id select r).Count()
                                     };

                    List<TBL_Music_Category> lst = new List<TBL_Music_Category>();

                    foreach (var category in categories)
                    {
                        category.cat.totalMusics = category.total;

                        lst.Add(category.cat);
                    }

                    return View("Index", lst);
                }

            }
            catch (Exception exception)
            {
                log.Error("Music/Category", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        public ActionResult Create()
        {
            return View();
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult Create(TBL_Music_Category category)
        {
            try
            {
                TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);

                category.register_person = registerPerson.row_id;
                category.update_person = registerPerson.row_id;
                category.register_datetime = DateTime.Now;
                category.update_datetime = DateTime.Now;

                if (ModelState.IsValid)
                {
                    int checkMusic = db.TBL_Music_Categorys.Where(c => c.category_name.Equals(category.category_name)).Count();

                    if (checkMusic > 0)
                    {
                        ModelState.AddModelError("category_name", "Category name is already exists! Please choose another");

                        return View(category);
                    }
                    else
                    {
                        db.TBL_Music_Categorys.Add(category);
                        db.SaveChanges();

                        TempData["MESSAGE"] = "Category is created successfully";
                        return RedirectToAction("Edit", new { id = category.row_id });
                    }
                }

                return View(category);
            }
            catch (Exception exception)
            {
                log.Error("Music Category/Create", exception);
                return View("Error");
            }

        }


        [AccountAuthorization]
        public ActionResult Edit(int id = 0)
        {
            try
            {
                ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                TBL_Music_Category category = db.TBL_Music_Categorys.Find(id);

                if (category != null)
                {
                    using (db)
                    {
                        var MusicCategoriesTran = from cat in db.TBL_Tran_Music_Categorys
                                                  join lang in db.TBL_Languages
                                                  on cat.language_id equals lang.row_id
                                                  where cat.category_row_id == category.row_id
                                                  select new
                                                  {
                                                      cat
                                                      ,
                                                      lang.language_name

                                                  };

                        List<TBL_Tran_Music_Category> MusicCats = new List<TBL_Tran_Music_Category>();

                        foreach (var MusicCat in MusicCategoriesTran)
                        {
                            MusicCat.cat.language_name = MusicCat.language_name;

                            MusicCats.Add(MusicCat.cat);
                        }


                        ViewBag.TranMusicCategories = MusicCats;
                    }

                    return View(category);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Music Category/Edit/id", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult Edit(TBL_Music_Category category)
        {
            try
            {
                using (db)
                {
                    ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                    var MusicCategoriesTran = from cat in db.TBL_Tran_Music_Categorys
                                              join lang in db.TBL_Languages
                                              on cat.language_id equals lang.row_id
                                              where cat.category_row_id == category.row_id
                                              select new
                                              {
                                                  cat
                                                  ,
                                                  lang.language_name

                                              };

                    List<TBL_Tran_Music_Category> MusicCats = new List<TBL_Tran_Music_Category>();

                    foreach (var MusicCat in MusicCategoriesTran)
                    {
                        MusicCat.cat.language_name = MusicCat.language_name;

                        MusicCats.Add(MusicCat.cat);
                    }


                    ViewBag.TranMusicCategories = MusicCats;


                    TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);

                    category.update_person = registerPerson.row_id;
                    category.update_datetime = DateTime.Now;

                    if (ModelState.IsValid)
                    {
                        int checkMusic = db.TBL_Music_Categorys.Where(c => c.category_name.Equals(category.category_name) && c.row_id != category.row_id).Count();

                        if (checkMusic > 0)
                        {
                            ModelState.AddModelError("category_name", "Category name is already exists! Please choose another");

                            return View(category);
                        }
                        else
                        {
                            db.Entry(category).State = EntityState.Modified;
                            db.SaveChanges();

                            TempData["MESSAGE"] = "Category is saved successfully";
                            return View(category);
                        }
                    }
                }

                return View(category);
            }
            catch (Exception exception)
            {
                log.Error("Music Category/Edit", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpGet]
        public ActionResult TranMusicCategory(int catid, int lang = 0)
        {
            try
            {
                if (catid > 0 && lang > 0)
                {
                    TBL_Tran_Music_Category tranMusicCategory = db.TBL_Tran_Music_Categorys.Where(g => g.language_id == lang && g.category_row_id == catid).SingleOrDefault();

                    if (tranMusicCategory != null)
                    {
                        return PartialView("TranMusicCategory", tranMusicCategory);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("Tran Music Category", exception);
                return View("Error");
            }
            return PartialView();
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult TranMusicCategory(TBL_Tran_Music_Category tranMusicCategory)
        {
            try
            {
                TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);


                tranMusicCategory.update_person = registerPerson.row_id;

                tranMusicCategory.update_datetime = DateTime.Now;

                if (tranMusicCategory.row_id == 0)
                {
                    tranMusicCategory.register_person = registerPerson.row_id;
                    tranMusicCategory.register_datetime = DateTime.Now;
                }

                //if (ModelState.IsValid)
                //{

                TBL_Tran_Music_Category existsTran = db.TBL_Tran_Music_Categorys.Where(t => t.language_id == tranMusicCategory.language_id && t.category_row_id == tranMusicCategory.category_row_id).SingleOrDefault();

                if (existsTran != null)
                {
                    existsTran.update_person = tranMusicCategory.update_person;
                    existsTran.update_datetime = tranMusicCategory.update_datetime;
                    existsTran.category_name = tranMusicCategory.category_name;
                    existsTran.status = tranMusicCategory.status;

                    db.Entry(existsTran).State = EntityState.Modified;

                    db.SaveChanges();
                }
                else
                {
                    db.TBL_Tran_Music_Categorys.Add(tranMusicCategory);
                    db.SaveChanges();
                }

                using (db)
                {
                    var MusicCategoriesTran = from cat in db.TBL_Tran_Music_Categorys
                                              join lang in db.TBL_Languages
                                              on cat.language_id equals lang.row_id
                                              where cat.category_row_id == tranMusicCategory.category_row_id
                                              select new
                                              {
                                                  cat.row_id
                                                  ,
                                                  lang.language_name
                                                  ,
                                                  cat.language_id
                                                  ,
                                                  cat.category_name
                                                  ,
                                                  cat.status
                                              };




                    return this.Json(MusicCategoriesTran.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Tran Music Category", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult DeleteTranMusicCategory(int catid, int langId = 0)
        {
            try
            {

                using (db)
                {
                    List<TBL_Tran_Music_Category> categories = (from cat in db.TBL_Tran_Music_Categorys
                                                                where cat.category_row_id == catid && cat.language_id == langId
                                                                select cat).ToList();

                    foreach (TBL_Tran_Music_Category tranMusicCat in categories)
                        db.TBL_Tran_Music_Categorys.Remove(tranMusicCat);

                    db.SaveChanges();


                    var MusicCategoriesTran = from cat in db.TBL_Tran_Music_Categorys
                                              join lang in db.TBL_Languages
                                              on cat.language_id equals lang.row_id
                                              where cat.category_row_id == catid
                                              select new
                                              {
                                                  cat.row_id
                                                  ,
                                                  lang.language_name
                                                  ,
                                                  cat.language_id
                                                  ,
                                                  cat.category_name
                                                  ,
                                                  cat.status
                                              };




                    return this.Json(MusicCategoriesTran.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Tran Music Category", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        public ActionResult Detail(int id = 0)
        {
            try
            {
                ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                TBL_Music_Category category = db.TBL_Music_Categorys.Find(id);

                if (category != null)
                {
                    using (db)
                    {
                        var MusicCategoriesTran = from cat in db.TBL_Tran_Music_Categorys
                                                  join lang in db.TBL_Languages
                                                  on cat.language_id equals lang.row_id
                                                  where cat.category_row_id == category.row_id
                                                  select new
                                                  {
                                                      cat
                                                      ,
                                                      lang.language_name

                                                  };

                        List<TBL_Tran_Music_Category> MusicCats = new List<TBL_Tran_Music_Category>();

                        foreach (var MusicCat in MusicCategoriesTran)
                        {
                            MusicCat.cat.language_name = MusicCat.language_name;

                            MusicCats.Add(MusicCat.cat);
                        }


                        ViewBag.TranMusicCategories = MusicCats;
                    }

                    return View(category);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Music Category/Detail/id", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                // Check if exists Music in category
                int checkExists = db.TBL_Category_Musics.Where(c => c.music_category_row_id == id).Count();

                if (checkExists > 0)
                {
                    TBL_Music_Category MusicCategory = db.TBL_Music_Categorys.Where(g => g.row_id == id).SingleOrDefault();

                    TBL_Account updatePerson = ((TBL_Account)Session["_ADMIN__"]);

                    if (MusicCategory != null)
                    {
                        MusicCategory.update_person = updatePerson.row_id;
                        MusicCategory.update_datetime = DateTime.Now;

                        MusicCategory.status = 9;

                        db.Entry(MusicCategory).State = EntityState.Modified;

                        db.SaveChanges();
                    }
                }
                else
                {
                    // Physical remove from db
                    // First remove Music translate
                    using (db)
                    {
                        var tranMusicCategories = from t in db.TBL_Tran_Music_Categorys
                                                  where t.category_row_id == id
                                                  select t;

                        foreach (TBL_Tran_Music_Category tranCat in tranMusicCategories)
                            db.Entry(tranCat).State = EntityState.Deleted;

                        db.SaveChanges();


                        var MusicCategories = from t in db.TBL_Music_Categorys
                                              where t.row_id == id
                                              select t;

                        foreach (TBL_Music_Category MusicCat in MusicCategories)
                            db.Entry(MusicCat).State = EntityState.Deleted;

                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Music Category/Delete/id", exception);
                return View("Error");
            }
        }
    }
}