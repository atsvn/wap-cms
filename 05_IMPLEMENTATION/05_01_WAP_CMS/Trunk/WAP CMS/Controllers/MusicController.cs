﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using log4net;
using WAP_CMS.Models;
using WAP_CMS.Common;

namespace WAP_CMS.Controllers
{
    public class MusicController : Controller
    {
        private CMSDbContext db = new CMSDbContext();

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Dictionary<string, HttpPostedFileBase> postedFileList = new Dictionary<string, HttpPostedFileBase>();

        [AccountAuthorization]
        public ActionResult Index(int catId = -1)
        {
            try
            {
                var categories = db.TBL_Music_Categorys.Where(c => c.status != 9);
                ViewBag.categories = categories.ToList();
                ViewBag.catId = catId;
                ViewBag.MusicCategory = new SelectList(categories.ToList(), "row_id", "category_name");
                List<TBL_Music> vList = new List<TBL_Music>();
                if (catId == -1)
                {
                    vList = db.TBL_Musics.Where(g => g.status != 9).OrderByDescending(g => g.update_datetime).ToList();
                }
                else
                {
                    var musicsWithCatFilter = from v in db.TBL_Musics
                                              join c in db.TBL_Category_Musics
                                              on v.row_id equals c.music_row_id
                                              where v.status != 9 && c.music_category_row_id == catId
                                              orderby v.update_datetime descending
                                              select new
                                              {
                                                  v
                                              };
                    foreach (var vid in musicsWithCatFilter)
                    {
                        vList.Add(vid.v);
                    }
                }


                var musicsList = new List<TBL_Music>();
                using (db)
                {
                    foreach (var music in vList)
                    {
                        music.categories = new List<TBL_Music_Category>();
                        var musicCategorys = from c in db.TBL_Music_Categorys
                                             join l in db.TBL_Category_Musics
                                             on c.row_id equals l.music_category_row_id
                                             where l.music_row_id == music.row_id && c.status != 9
                                             select new
                                             {
                                                 c
                                             };

                        foreach (var category in musicCategorys)
                        {
                            music.categories.Add(category.c);
                        }
                        musicsList.Add(music);
                    }
                }

                return View("Index", musicsList);
            }
            catch (Exception exception)
            {
                log.Error("Music/Index", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        public ActionResult Create()
        {
            return View();
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Create(TBL_Music music)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    bool isError = false;
                    HttpFileCollectionBase postedFiles = Request.Files;

                    for (int i = 0; i < postedFiles.Count; i++)
                    {
                        HttpPostedFileBase file = postedFiles[i];
                        string fileName = file.FileName.ToLower();


                        switch (postedFiles.GetKey(i))
                        {

                            case "music_file":
                                if (fileName.Length > 0)
                                {
                                    if (file.ContentType.Contains("audio"))
                                    {
                                        postedFileList.Add("music_file", file);
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("music_file", "The Music field must be mp3 file.");
                                        isError = true;
                                    }
                                }
                                else
                                {
                                    postedFileList.Remove("music_file");
                                }

                                break;
                            case "amr_file":
                                if (fileName.Length > 0)
                                {
                                    if (file.ContentType.Contains(""))
                                    {
                                        postedFileList.Add("amr_file", file);
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("amr_file", "");
                                        isError = true;
                                    }
                                }
                                else
                                {
                                    postedFileList.Remove("amr_file");
                                }

                                break;
                            case "mmf_file":
                                if (fileName.Length > 0)
                                {
                                    if (file.ContentType.Contains(""))
                                    {
                                        postedFileList.Add("mmf_file", file);
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("mmf_file", "");
                                        isError = true;
                                    }
                                }
                                else
                                {
                                    postedFileList.Remove("mmf_file");
                                }

                                break;
                            default:
                                break;
                        }
                    }

                    // File upload error
                    if (isError)
                    {
                        return View(music);
                    }
                    else
                    {
                        foreach (var item in postedFileList)
                        {
                            HttpPostedFileBase file = item.Value;

                            switch (item.Key)
                            {

                                case "music_file":
                                    music.music_file = Util.FtpUploadMusic(file);
                                    break;
                                case "amr_file":
                                    music.amr_file = Util.FtpUploadMusicAmr(file);
                                    break;
                                case "mmf_file":
                                    music.mmf_file = Util.FtpUploadMusicMmf(file);
                                    break;


                            }
                        }

                        TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);

                        music.register_person = registerPerson.row_id;
                        music.update_person = registerPerson.row_id;

                        music.register_datetime = DateTime.Now;
                        music.update_datetime = DateTime.Now;

                        db.TBL_Musics.Add(music);
                        db.SaveChanges();

                        TempData["MESSAGE"] = "Your music is created successfully";
                        return RedirectToAction("Edit", new { id = music.row_id });
                    }


                }
                catch (Exception exception)
                {
                    log.Error("Music/Create", exception);
                    return View("Error");
                }
            }

            return View(music);
        }

        [AccountAuthorization]
        public ActionResult Edit(int id = 0)
        {
            try
            {
                TBL_Music music = db.TBL_Musics.Find(id);

                if (music != null)
                {
                    ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                    using (db)
                    {
                        var musicTrans = from g in db.TBL_Tran_Musics
                                         join lang in db.TBL_Languages
                                         on g.language_id equals lang.row_id
                                         where g.music_row_id == music.row_id
                                         select new
                                         {
                                             g
                                             ,
                                             lang.language_name

                                         };

                        List<TBL_Tran_Music> musics = new List<TBL_Tran_Music>();

                        foreach (var musicTran in musicTrans)
                        {
                            musicTran.g.language_name = musicTran.language_name;

                            musics.Add(musicTran.g);
                        }


                        ViewBag.TranMusics = musics;


                        music.allCategories = db.TBL_Music_Categorys.Where(c => c.status != 9).ToList();

                        var musicCategorys = from c in db.TBL_Music_Categorys
                                             join l in db.TBL_Category_Musics
                                             on c.row_id equals l.music_category_row_id
                                             where l.music_row_id == music.row_id && c.status != 9
                                             select new
                                             {
                                                 c
                                             };

                        music.categories = new List<TBL_Music_Category>();
                        foreach (var category in musicCategorys)
                        {
                            music.categories.Add(category.c);
                        }
                    }

                    return View(music);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Music/Edit/id", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Edit(TBL_Music music)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (db)
                    {
                        ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                        var musicTrans = from g in db.TBL_Tran_Musics
                                         join lang in db.TBL_Languages
                                         on g.language_id equals lang.row_id
                                         where g.music_row_id == music.row_id
                                         select new
                                         {
                                             g
                                             ,
                                             lang.language_name

                                         };

                        List<TBL_Tran_Music> musics = new List<TBL_Tran_Music>();

                        foreach (var musicTran in musicTrans)
                        {
                            musicTran.g.language_name = musicTran.language_name;

                            musics.Add(musicTran.g);
                        }


                        ViewBag.TranMusics = musics;


                        bool isError = false;
                        HttpFileCollectionBase postedFiles = Request.Files;

                        for (int i = 0; i < postedFiles.Count; i++)
                        {
                            HttpPostedFileBase file = postedFiles[i];
                            string fileName = file.FileName.ToLower();


                            switch (postedFiles.GetKey(i))
                            {
                                case "music_file":
                                    if (fileName.Length > 0)
                                    {
                                        
                                        if (file.ContentType.Contains(""))
                                        {
                                            postedFileList.Add("music_file", file);
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("music_file", "The Music field must be mp3 file.");
                                            isError = true;
                                        }
                                    }
                                    else
                                    {
                                        postedFileList.Remove("music_file");
                                    }

                                    break;
                                case "amr_file":
                                    if (fileName.Length > 0)
                                    {

                                        if (file.ContentType.Contains(""))
                                        {
                                            postedFileList.Add("amr_file", file);
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("amr_file", "The Music field must be audio file.");
                                            isError = true;
                                        }
                                    }
                                    else
                                    {
                                        postedFileList.Remove("amr_file");
                                    }

                                    break;
                                case "mmf_file":
                                    if (fileName.Length > 0)
                                    {
                                        
                                        if (file.ContentType.Contains(""))
                                        {
                                            postedFileList.Add("mmf_file", file);
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("mmf_file", "");
                                            isError = true;
                                        }
                                    }
                                    else
                                    {
                                        postedFileList.Remove("mmf_file");
                                    }

                                    break;
                                default:
                                    break;
                            }
                        }

                        // File upload error
                        if (isError)
                        {
                            music.allCategories = db.TBL_Music_Categorys.Where(c => c.status != 9).ToList();

                            var musicCategorys = from c in db.TBL_Music_Categorys
                                                 join l in db.TBL_Category_Musics
                                                 on c.row_id equals l.music_category_row_id
                                                 where l.music_row_id == music.row_id && c.status != 9
                                                 select new
                                                 {
                                                     c
                                                 };

                            music.categories = new List<TBL_Music_Category>();
                            foreach (var category in musicCategorys)
                            {
                                music.categories.Add(category.c);
                            }

                            return View(music);
                        }
                        else
                        {
                            foreach (var item in postedFileList)
                            {
                                HttpPostedFileBase file = item.Value;

                                switch (item.Key)
                                {
                                    case "music_file":
                                        music.music_file = Util.FtpUploadMusic(file);
                                        break;
                                    case "amr_file":
                                        music.amr_file = Util.FtpUploadMusicAmr(file);
                                        break;
                                    case "mmf_file":
                                        music.mmf_file = Util.FtpUploadMusicMmf(file);
                                        break;
                                }
                            }

                            TBL_Account updatePerson = ((TBL_Account)Session["_ADMIN__"]);

                            music.update_person = updatePerson.row_id;
                            music.update_datetime = DateTime.Now;

                            db.Entry(music).State = EntityState.Modified;
                            db.SaveChanges();

                            // Remove old game category
                            List<TBL_Category_Music> categoryMusics = (from c in db.TBL_Category_Musics
                                                                       where c.music_row_id == music.row_id
                                                                       select c).ToList();

                            if (categoryMusics.Count > 0)
                            {
                                foreach (TBL_Category_Music ob in categoryMusics)
                                    db.TBL_Category_Musics.Remove(ob);

                                db.SaveChanges();
                            }

                            // Add news
                            if (music.PostedCategoryRowIds != null && music.PostedCategoryRowIds.Length > 0)
                            {
                                for (int i = 0; i < music.PostedCategoryRowIds.Length; i++)
                                {
                                    TBL_Category_Music cg = new TBL_Category_Music();
                                    cg.music_row_id = music.row_id;
                                    cg.music_category_row_id = Convert.ToInt32(music.PostedCategoryRowIds[i]);

                                    db.TBL_Category_Musics.Add(cg);
                                }
                                db.SaveChanges();
                            }


                            music.allCategories = db.TBL_Music_Categorys.Where(c => c.status != 9).ToList();

                            var musicCategorys = from c in db.TBL_Music_Categorys
                                                 join l in db.TBL_Category_Musics
                                                 on c.row_id equals l.music_category_row_id
                                                 where l.music_row_id == music.row_id && c.status != 9
                                                 select new
                                                 {
                                                     c
                                                 };

                            music.categories = new List<TBL_Music_Category>();
                            foreach (var category in musicCategorys)
                            {
                                music.categories.Add(category.c);
                            }


                            TempData["MESSAGE"] = "Music is saved successfully";
                            return View(music);
                        }
                    }
                }
                catch (Exception exception)
                {
                    log.Error("Music/Edit", exception);
                    return View("Error");
                }
            }

            return View(music);
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                TBL_Account deletePerson = ((TBL_Account)Session["_ADMIN__"]);

                TBL_Music music = db.TBL_Musics.Find(id);

                if (music != null)
                {
                    music.update_person = deletePerson.row_id;
                    music.update_datetime = DateTime.Now;

                    music.status = 9;

                    db.Entry(music).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Music/Delete", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        [HttpGet]
        public ActionResult TranMusic(int musicid, int lang = 0)
        {
            try
            {
                if (musicid > 0 && lang > 0)
                {
                    TBL_Tran_Music tranMusic = db.TBL_Tran_Musics.Where(g => g.language_id == lang && g.music_row_id == musicid).SingleOrDefault();

                    if (tranMusic != null)
                    {
                        return PartialView("TranMusic", tranMusic);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("Tran Music", exception);
                return View("Error");
            }
            return PartialView();
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult TranMusic(TBL_Tran_Music tranMusic)
        {
            try
            {
                TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);


                tranMusic.update_person = registerPerson.row_id;

                tranMusic.update_datetime = DateTime.Now;

                if (tranMusic.row_id == 0)
                {
                    tranMusic.register_person = registerPerson.row_id;
                    tranMusic.register_datetime = DateTime.Now;
                }

                //if (ModelState.IsValid)
                //{

                TBL_Tran_Music existsTran = db.TBL_Tran_Musics.Where(t => t.language_id == tranMusic.language_id && t.music_row_id == tranMusic.music_row_id).SingleOrDefault();

                if (existsTran != null)
                {
                    existsTran.update_person = tranMusic.update_person;
                    existsTran.update_datetime = tranMusic.update_datetime;

                    existsTran.name = tranMusic.name;
                    existsTran.short_description = tranMusic.short_description;
                   

                    existsTran.status = tranMusic.status;

                    db.Entry(existsTran).State = EntityState.Modified;

                    db.SaveChanges();
                }
                else
                {
                    db.TBL_Tran_Musics.Add(tranMusic);
                    db.SaveChanges();
                }

                using (db)
                {
                    var musics = from g in db.TBL_Tran_Musics
                                 join lang in db.TBL_Languages
                                 on g.language_id equals lang.row_id
                                 where g.music_row_id == tranMusic.music_row_id
                                 select new
                                 {
                                     g.row_id
                                     ,
                                     lang.language_name
                                     ,
                                     g.language_id
                                     ,
                                     g.name
                                     ,
                                     g.status
                                 };




                    return this.Json(musics.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Tran Music", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult DeleteTranMusic(int musicid, int langId = 0)
        {
            try
            {

                using (db)
                {
                    List<TBL_Tran_Music> tranMusics = (from g in db.TBL_Tran_Musics
                                                       where g.music_row_id == musicid && g.language_id == langId
                                                       select g).ToList();

                    foreach (TBL_Tran_Music tranMusic in tranMusics)
                        db.TBL_Tran_Musics.Remove(tranMusic);

                    db.SaveChanges();


                    var musics = from g in db.TBL_Tran_Musics
                                 join lang in db.TBL_Languages
                                 on g.language_id equals lang.row_id
                                 where g.music_row_id == musicid
                                 select new
                                 {
                                     g.row_id
                                     ,
                                     lang.language_name
                                     ,
                                     g.language_id
                                     ,
                                     g.name
                                     ,
                                     g.status
                                 };




                    return this.Json(musics.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Delete Tran Music", exception);
                return View("Error");
            }
        }
    }



}