﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using log4net;
using WAP_CMS.Models;
using System.Web.Script.Serialization;

namespace WAP_CMS.Controllers
{
    public class PhotoCategoryController : Controller
    {
        private CMSDbContext db = new CMSDbContext();

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [AccountAuthorization]
        public ActionResult Index()
        {
            try
            {
                //var categories = db.TBL_Photo_Categorys.Where(c => c.status != 9).OrderBy(c => c.category_name);
                using (db)
                {
                    var categories = from cat in db.TBL_Photo_Categorys
                                     where cat.status != 9
                                     select new
                                     {
                                         cat,
                                         total = (from r in db.TBL_Category_Photos where r.photo_category_row_id == cat.row_id select r).Count()
                                     };

                    List<TBL_Photo_Category> lst = new List<TBL_Photo_Category>();

                    foreach (var category in categories)
                    {
                        category.cat.totalPhotos = category.total;

                        lst.Add(category.cat);
                    }

                    return View("Index", lst);
                }

            }
            catch (Exception exception)
            {
                log.Error("Photo/Category", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        public ActionResult Create()
        {
            return View();
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult Create(TBL_Photo_Category category)
        {
            try
            {
                TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);

                category.register_person = registerPerson.row_id;
                category.update_person = registerPerson.row_id;
                category.register_datetime = DateTime.Now;
                category.update_datetime = DateTime.Now;

                if (ModelState.IsValid)
                {
                    int checkPhoto = db.TBL_Photo_Categorys.Where(c => c.category_name.Equals(category.category_name)).Count();

                    if (checkPhoto > 0)
                    {
                        ModelState.AddModelError("category_name", "Category name is already exists! Please choose another");

                        return View(category);
                    }
                    else
                    {
                        db.TBL_Photo_Categorys.Add(category);
                        db.SaveChanges();

                        TempData["MESSAGE"] = "Category is created successfully";
                        return RedirectToAction("Edit", new { id = category.row_id });
                    }
                }

                return View(category);
            }
            catch (Exception exception)
            {
                log.Error("Photo Category/Create", exception);
                return View("Error");
            }

        }


        [AccountAuthorization]
        public ActionResult Edit(int id = 0)
        {
            try
            {
                ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                TBL_Photo_Category category = db.TBL_Photo_Categorys.Find(id);

                if (category != null)
                {
                    using (db)
                    {
                        var photoCategoriesTran = from cat in db.TBL_Tran_Photo_Categorys
                                                 join lang in db.TBL_Languages
                                                 on cat.language_id equals lang.row_id
                                                 where cat.category_row_id == category.row_id
                                                 select new
                                                 {
                                                     cat
                                                     ,
                                                     lang.language_name

                                                 };

                        List<TBL_Tran_Photo_Category> photoCats = new List<TBL_Tran_Photo_Category>();

                        foreach (var photoCat in photoCategoriesTran)
                        {
                            photoCat.cat.language_name = photoCat.language_name;

                            photoCats.Add(photoCat.cat);
                        }


                        ViewBag.TranPhotoCategories = photoCats;
                    }

                    return View(category);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Photo Category/Edit/id", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult Edit(TBL_Photo_Category category)
        {
            try
            {
                using (db)
                {
                    ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                    var photoCategoriesTran = from cat in db.TBL_Tran_Photo_Categorys
                                             join lang in db.TBL_Languages
                                             on cat.language_id equals lang.row_id
                                             where cat.category_row_id == category.row_id
                                             select new
                                             {
                                                 cat
                                                 ,
                                                 lang.language_name

                                             };

                    List<TBL_Tran_Photo_Category> photoCats = new List<TBL_Tran_Photo_Category>();

                    foreach (var photoCat in photoCategoriesTran)
                    {
                        photoCat.cat.language_name = photoCat.language_name;

                        photoCats.Add(photoCat.cat);
                    }


                    ViewBag.TranPhotoCategories = photoCats;


                    TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);

                    category.update_person = registerPerson.row_id;
                    category.update_datetime = DateTime.Now;

                    if (ModelState.IsValid)
                    {
                        int checkPhoto = db.TBL_Photo_Categorys.Where(c => c.category_name.Equals(category.category_name) && c.row_id != category.row_id).Count();

                        if (checkPhoto > 0)
                        {
                            ModelState.AddModelError("category_name", "Category name is already exists! Please choose another");

                            return View(category);
                        }
                        else
                        {
                            db.Entry(category).State = EntityState.Modified;
                            db.SaveChanges();

                            TempData["MESSAGE"] = "Category is saved successfully";
                            return View(category);
                        }
                    }
                }

                return View(category);
            }
            catch (Exception exception)
            {
                log.Error("Photo Category/Edit", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpGet]
        public ActionResult TranPhotoCategory(int catid, int lang = 0)
        {
            try
            {
                if (catid > 0 && lang > 0)
                {
                    TBL_Tran_Photo_Category tranPhotoCategory = db.TBL_Tran_Photo_Categorys.Where(g => g.language_id == lang && g.category_row_id == catid).SingleOrDefault();

                    if (tranPhotoCategory != null)
                    {
                        return PartialView("TranPhotoCategory", tranPhotoCategory);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("Tran Photo Category", exception);
                return View("Error");
            }
            return PartialView();
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult TranPhotoCategory(TBL_Tran_Photo_Category tranPhotoCategory)
        {
            try
            {
                TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);


                tranPhotoCategory.update_person = registerPerson.row_id;

                tranPhotoCategory.update_datetime = DateTime.Now;

                if (tranPhotoCategory.row_id == 0)
                {
                    tranPhotoCategory.register_person = registerPerson.row_id;
                    tranPhotoCategory.register_datetime = DateTime.Now;
                }

                //if (ModelState.IsValid)
                //{

                TBL_Tran_Photo_Category existsTran = db.TBL_Tran_Photo_Categorys.Where(t => t.language_id == tranPhotoCategory.language_id && t.category_row_id == tranPhotoCategory.category_row_id).SingleOrDefault();

                if (existsTran != null)
                {
                    existsTran.update_person = tranPhotoCategory.update_person;
                    existsTran.update_datetime = tranPhotoCategory.update_datetime;
                    existsTran.category_name = tranPhotoCategory.category_name;
                    existsTran.status = tranPhotoCategory.status;

                    db.Entry(existsTran).State = EntityState.Modified;

                    db.SaveChanges();
                }
                else
                {
                    db.TBL_Tran_Photo_Categorys.Add(tranPhotoCategory);
                    db.SaveChanges();
                }

                using (db)
                {
                    var photoCategoriesTran = from cat in db.TBL_Tran_Photo_Categorys
                                             join lang in db.TBL_Languages
                                             on cat.language_id equals lang.row_id
                                             where cat.category_row_id == tranPhotoCategory.category_row_id
                                             select new
                                             {
                                                 cat.row_id
                                                 ,
                                                 lang.language_name
                                                 ,
                                                 cat.language_id
                                                 ,
                                                 cat.category_name
                                                 ,
                                                 cat.status
                                             };




                    return this.Json(photoCategoriesTran.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Tran Photo Category", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult DeleteTranPhotoCategory(int catid, int langId = 0)
        {
            try
            {

                using (db)
                {
                    List<TBL_Tran_Photo_Category> categories = (from cat in db.TBL_Tran_Photo_Categorys
                                                               where cat.category_row_id == catid && cat.language_id == langId
                                                               select cat).ToList();

                    foreach (TBL_Tran_Photo_Category tranPhotoCat in categories)
                        db.TBL_Tran_Photo_Categorys.Remove(tranPhotoCat);

                    db.SaveChanges();


                    var photoCategoriesTran = from cat in db.TBL_Tran_Photo_Categorys
                                             join lang in db.TBL_Languages
                                             on cat.language_id equals lang.row_id
                                             where cat.category_row_id == catid
                                             select new
                                             {
                                                 cat.row_id
                                                 ,
                                                 lang.language_name
                                                 ,
                                                 cat.language_id
                                                 ,
                                                 cat.category_name
                                                 ,
                                                 cat.status
                                             };




                    return this.Json(photoCategoriesTran.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Tran Photo Category", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        public ActionResult Detail(int id = 0)
        {
            try
            {
                ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                TBL_Photo_Category category = db.TBL_Photo_Categorys.Find(id);

                if (category != null)
                {
                    using (db)
                    {
                        var photoCategoriesTran = from cat in db.TBL_Tran_Photo_Categorys
                                                 join lang in db.TBL_Languages
                                                 on cat.language_id equals lang.row_id
                                                 where cat.category_row_id == category.row_id
                                                 select new
                                                 {
                                                     cat
                                                     ,
                                                     lang.language_name

                                                 };

                        List<TBL_Tran_Photo_Category> photoCats = new List<TBL_Tran_Photo_Category>();

                        foreach (var photoCat in photoCategoriesTran)
                        {
                            photoCat.cat.language_name = photoCat.language_name;

                            photoCats.Add(photoCat.cat);
                        }


                        ViewBag.TranPhotoCategories = photoCats;
                    }

                    return View(category);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Photo Category/Detail/id", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                // Check if exists photo in category
                int checkExists = db.TBL_Category_Photos.Where(c => c.photo_category_row_id == id).Count();

                if (checkExists > 0)
                {
                    TBL_Photo_Category photoCategory = db.TBL_Photo_Categorys.Where(g => g.row_id == id).SingleOrDefault();

                    TBL_Account updatePerson = ((TBL_Account)Session["_ADMIN__"]);

                    if (photoCategory != null)
                    {
                        photoCategory.update_person = updatePerson.row_id;
                        photoCategory.update_datetime = DateTime.Now;

                        photoCategory.status = 9;

                        db.Entry(photoCategory).State = EntityState.Modified;

                        db.SaveChanges();
                    }
                }
                else
                {
                    // Physical remove from db
                    // First remove photo translate
                    using (db)
                    {
                        var tranPhotoCategories = from t in db.TBL_Tran_Photo_Categorys
                                                 where t.category_row_id == id
                                                 select t;

                        foreach (TBL_Tran_Photo_Category tranCat in tranPhotoCategories)
                            db.Entry(tranCat).State = EntityState.Deleted;

                        db.SaveChanges();


                        var photoCategories = from t in db.TBL_Photo_Categorys
                                             where t.row_id == id
                                             select t;

                        foreach (TBL_Photo_Category photoCat in photoCategories)
                            db.Entry(photoCat).State = EntityState.Deleted;

                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Photo Category/Delete/id", exception);
                return View("Error");
            }
        }

    }
}
