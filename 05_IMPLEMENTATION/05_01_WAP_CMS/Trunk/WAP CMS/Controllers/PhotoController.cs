﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using log4net;
using WAP_CMS.Models;
using WAP_CMS.Common;

namespace WAP_CMS.Controllers
{
    public class PhotoController : Controller
    {
        private CMSDbContext db = new CMSDbContext();

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Dictionary<string, HttpPostedFileBase> postedFileList = new Dictionary<string, HttpPostedFileBase>();

        [AccountAuthorization]
        public ActionResult Index()
        {
            try
            {
                var categories = db.TBL_Photo_Categorys.Where(c => c.status != 9);

                ViewBag.PhotoCategory = new SelectList(categories.ToList(), "row_id", "category_name");

                var photos = db.TBL_Photos.Where(g => g.status != 9).Take(1);
                return View("Index", photos.ToList());
            }
            catch (Exception exception)
            {
                log.Error("Photo/Index", exception);
                return View("Error");
            }
        }

        public ActionResult IndexJson(JQueryDataTableParamModel param)
        {
            
            try
            {
                IEnumerable<TBL_Photo> filteredList;
                int iDisplayStart = int.Parse(Request.Params["iDisplayStart"]);
                int iDisplayLength = int.Parse(Request.Params["iDisplayLength"]);
                int iTotalRecord = 0;
                int iTotalDisplayRecords = 0;
                int iSortCol_0 = int.Parse(Request.Params["iSortCol_0"]);
                string sSortDir_0 = Request.Params["sSortDir_0"];
                

                //queries and filter by search
                var photos = db.TBL_Photos.Where(p => p.status != 9);
                iTotalRecord = photos.Count();
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    filteredList = photos.Where(
                            p => p.name.Contains(param.sSearch)
                        ).OrderBy(p => p.row_id);
                    iTotalDisplayRecords = photos.Where(
                            p => p.name.Contains(param.sSearch)
                        ).Count();
                }
                else
                {
                    filteredList = photos;
                    iTotalDisplayRecords = iTotalRecord;
                }


                //Check sort criteria and sort filteredList
                Func<TBL_Photo, string> orderingFunction = (p =>    
                                                                    iSortCol_0 == 1 ? p.name :
                                                                    iSortCol_0 == 2 ? Convert.ToString(p.view) :
                                                                    iSortCol_0 == 3 ? Convert.ToString(p.download) :                                                           
                                                            "");
                if (sSortDir_0 == "asc")
                    filteredList = filteredList.OrderBy(orderingFunction);
                else
                    filteredList = filteredList.OrderByDescending(orderingFunction);
                                                            
                //pagination
                iTotalDisplayRecords = filteredList.Count();
                filteredList = filteredList.Skip(param.iDisplayStart).Take(iDisplayLength);
                


                //indexing results and convert to Array of String object
                List<Array> result = new List<Array>();
                for(int i = 0; i < filteredList.Count(); i ++ ){
                    var p = filteredList.ElementAt(i);
                    string[] res = new string[] {Convert.ToString(iDisplayStart + i + 1), p.name, Convert.ToString(p.view), Convert.ToString(p.download), Convert.ToString(p.Enable), Convert.ToString(p.row_id), Convert.ToString(p.row_id) };
                    result.Add(res);
                }
                
                var jsonResult = Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = iTotalRecord,
                    iTotalDisplayRecords = iTotalDisplayRecords,
                    aaData = result
                }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                
                return jsonResult;
            }
            catch (Exception exception)
            {
                log.Error("Photo/Index", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        public ActionResult Create()
        {
            return View();
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Create(TBL_Photo photo)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    bool isError = false;
                    HttpFileCollectionBase postedFiles = Request.Files;

                    for (int i = 0; i < postedFiles.Count; i++)
                    {
                        HttpPostedFileBase file = postedFiles[i];
                        string fileName = file.FileName.ToLower();


                        switch (postedFiles.GetKey(i))
                        {
                            case "Image":
                                if (fileName.Length > 0)
                                {
                                    if (file.ContentType.Contains("image"))
                                    {
                                        postedFileList.Add("Image", file);
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("ImageURL", "The Image field must be image.");
                                        isError = true;
                                    }
                                }
                                else
                                {
                                    postedFileList.Remove("Image");
                                }

                                break;

                            case "Image-ota":
                                if (fileName.Length > 0)
                                {
                                    //if (file.ContentType.Contains("image"))
                                    //{
                                        
                                    //}
                                    //else
                                    //{
                                    //    ModelState.AddModelError("ImageURL", "The Image field must be image.");
                                    //    isError = true;
                                    //}
                                    postedFileList.Add("Image-ota", file);
                                }
                                else
                                {
                                    postedFileList.Remove("Image-ota");
                                }

                                break;

                            default:
                                break;
                        }
                    }

                    // File upload error
                    if (isError)
                    {
                        return View(photo);
                    }
                    else
                    {
                        foreach (var item in postedFileList)
                        {
                            HttpPostedFileBase file = item.Value;

                            switch (item.Key)
                            {
                                case "Image":
                                    photo.img = Util.FtpUploadPhotoImg(new WebImage(file.InputStream));

                                    break;
                                case "Image-ota":
                                    photo.img_ota = Util.FtpUploadPhotoOta(file);

                                    break;

                                //case "AndroidApp":
                                //    photo.androidappsize = file.ContentLength;
                                //    photo.androidappurl = Util.FtpUploadPhoto(file);

                                //    break;




                                    
                            }
                        }

                        TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);

                        photo.register_person = registerPerson.row_id;
                        photo.update_person = registerPerson.row_id;

                        photo.register_datetime = DateTime.Now;
                        photo.update_datetime = DateTime.Now;

                        db.TBL_Photos.Add(photo);
                        db.SaveChanges();

                        TempData["MESSAGE"] = "Photo is created successfully";
                        return RedirectToAction("Edit", new { id = photo.row_id });
                    }


                }
                catch (Exception exception)
                {
                    log.Error("Photo/Create", exception);
                    return View("Error");
                }
            }

            return View(photo);
        }

        [AccountAuthorization]
        public ActionResult Edit(int id = 0)
        {
            try
            {
                TBL_Photo photo = db.TBL_Photos.Find(id);

                if (photo != null)
                {
                    ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                    using (db)
                    {
                        var photoTrans = from g in db.TBL_Tran_Photos
                                        join lang in db.TBL_Languages
                                        on g.language_id equals lang.row_id
                                        where g.photo_row_id == photo.row_id
                                        select new
                                        {
                                            g
                                            ,
                                            lang.language_name

                                        };

                        List<TBL_Tran_Photo> photos = new List<TBL_Tran_Photo>();

                        foreach (var photoTran in photoTrans)
                        {
                            photoTran.g.language_name = photoTran.language_name;

                            photos.Add(photoTran.g);
                        }


                        ViewBag.TranPhotos = photos;


                        photo.allCategories = db.TBL_Photo_Categorys.Where(c => c.status != 9).ToList();

                        var photoCategorys = from c in db.TBL_Photo_Categorys
                                            join l in db.TBL_Category_Photos
                                            on c.row_id equals l.photo_category_row_id
                                            where l.photo_row_id == photo.row_id && c.status != 9
                                            select new
                                            {
                                                c
                                            };

                        photo.categories = new List<TBL_Photo_Category>();
                        foreach (var category in photoCategorys)
                        {
                            photo.categories.Add(category.c);
                        }
                    }

                    return View(photo);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Photo/Edit/id", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Edit(TBL_Photo photo)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (db)
                    {
                        ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                        var photoTrans = from g in db.TBL_Tran_Photos
                                        join lang in db.TBL_Languages
                                        on g.language_id equals lang.row_id
                                        where g.photo_row_id == photo.row_id
                                        select new
                                        {
                                            g
                                            ,
                                            lang.language_name

                                        };

                        List<TBL_Tran_Photo> photos = new List<TBL_Tran_Photo>();

                        foreach (var photoTran in photoTrans)
                        {
                            photoTran.g.language_name = photoTran.language_name;

                            photos.Add(photoTran.g);
                        }


                        ViewBag.TranPhotos = photos;


                        bool isError = false;
                        HttpFileCollectionBase postedFiles = Request.Files;

                        for (int i = 0; i < postedFiles.Count; i++)
                        {
                            HttpPostedFileBase file = postedFiles[i];
                            string fileName = file.FileName.ToLower();


                            switch (postedFiles.GetKey(i))
                            {
                                case "Image":
                                    if (fileName.Length > 0)
                                    {
                                        if (file.ContentType.Contains("image"))
                                        {
                                            postedFileList.Add("Image", file);
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("ImageURL", "The Image field must be image.");
                                            isError = true;
                                        }
                                    }
                                    else
                                    {
                                        postedFileList.Remove("Image");
                                    }

                                    break;
                                case "Image-ota":
                                    if (fileName.Length > 0)
                                    {
                                        postedFileList.Add("Image-ota", file);
                                        //if (file.ContentType.Contains("image"))
                                        //{
                                            
                                        //}
                                        //else
                                        //{
                                        //    ModelState.AddModelError("ImageURL", "The Image field must be image.");
                                        //    isError = true;
                                        //}
                                    }
                                    else
                                    {
                                        postedFileList.Remove("Image-ota");
                                    }

                                    break;

                                

                                default:
                                    break;
                            }
                        }

                        // File upload error
                        if (isError)
                        {
                            photo.allCategories = db.TBL_Photo_Categorys.Where(c => c.status != 9).ToList();

                            var photoCategorys = from c in db.TBL_Photo_Categorys
                                                join l in db.TBL_Category_Photos
                                                on c.row_id equals l.photo_category_row_id
                                                where l.photo_row_id == photo.row_id && c.status != 9
                                                select new
                                                {
                                                    c
                                                };

                            photo.categories = new List<TBL_Photo_Category>();
                            foreach (var category in photoCategorys)
                            {
                                photo.categories.Add(category.c);
                            }

                            return View(photo);
                        }
                        else
                        {
                            foreach (var item in postedFileList)
                            {
                                HttpPostedFileBase file = item.Value;

                                switch (item.Key)
                                {
                                    case "Image":
                                        photo.img = Util.FtpUploadPhotoImg(new WebImage(file.InputStream));

                                        break;
                                    case "Image-ota":
                                        photo.img_ota = Util.FtpUploadPhotoOta(file);

                                        break;

                                    
                                }
                            }

                            TBL_Account updatePerson = ((TBL_Account)Session["_ADMIN__"]);

                            photo.update_person = updatePerson.row_id;
                            photo.update_datetime = DateTime.Now;

                            db.Entry(photo).State = EntityState.Modified;
                            db.SaveChanges();

                            // Remove old photo category
                            List<TBL_Category_Photo> categoryPhotos = (from c in db.TBL_Category_Photos
                                                                     where c.photo_row_id == photo.row_id
                                                                     select c).ToList();

                            if (categoryPhotos.Count > 0)
                            {
                                foreach (TBL_Category_Photo ob in categoryPhotos)
                                    db.TBL_Category_Photos.Remove(ob);

                                db.SaveChanges();
                            }

                            // Add news
                            if (photo.PostedCategoryRowIds != null && photo.PostedCategoryRowIds.Length > 0)
                            {
                                for (int i = 0; i < photo.PostedCategoryRowIds.Length; i++)
                                {
                                    TBL_Category_Photo cg = new TBL_Category_Photo();
                                    cg.photo_row_id = photo.row_id;
                                    cg.photo_category_row_id = Convert.ToInt32(photo.PostedCategoryRowIds[i]);

                                    db.TBL_Category_Photos.Add(cg);
                                }
                                db.SaveChanges();
                            }


                            photo.allCategories = db.TBL_Photo_Categorys.Where(c => c.status != 9).ToList();

                            var photoCategorys = from c in db.TBL_Photo_Categorys
                                                join l in db.TBL_Category_Photos
                                                on c.row_id equals l.photo_category_row_id
                                                where l.photo_row_id == photo.row_id && c.status != 9
                                                select new
                                                {
                                                    c
                                                };

                            photo.categories = new List<TBL_Photo_Category>();
                            foreach (var category in photoCategorys)
                            {
                                photo.categories.Add(category.c);
                            }


                            TempData["MESSAGE"] = "Photo is saved successfully";
                            return View(photo);
                        }
                    }
                }
                catch (Exception exception)
                {
                    log.Error("Photo/Edit", exception);
                    return View("Error");
                }
            }

            return View(photo);
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                TBL_Account deletePerson = ((TBL_Account)Session["_ADMIN__"]);

                TBL_Photo photo = db.TBL_Photos.Find(id);

                if (photo != null)
                {
                    photo.update_person = deletePerson.row_id;
                    photo.update_datetime = DateTime.Now;

                    photo.status = 9;

                    db.Entry(photo).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Photo/Delete", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpGet]
        public ActionResult TranPhoto(int photoid, int lang = 0)
        {
            try
            {
                if (photoid > 0 && lang > 0)
                {
                    TBL_Tran_Photo tranPhoto = db.TBL_Tran_Photos.Where(g => g.language_id == lang && g.photo_row_id == photoid).SingleOrDefault();

                    if (tranPhoto != null)
                    {
                        return PartialView("TranPhoto", tranPhoto);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("Tran Photo", exception);
                return View("Error");
            }
            return PartialView();
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult TranPhoto(TBL_Tran_Photo tranPhoto)
        {
            try
            {
                TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);


                tranPhoto.update_person = registerPerson.row_id;

                tranPhoto.update_datetime = DateTime.Now;

                if (tranPhoto.row_id == 0)
                {
                    tranPhoto.register_person = registerPerson.row_id;
                    tranPhoto.register_datetime = DateTime.Now;
                }

                //if (ModelState.IsValid)
                //{

                TBL_Tran_Photo existsTran = db.TBL_Tran_Photos.Where(t => t.language_id == tranPhoto.language_id && t.photo_row_id == tranPhoto.photo_row_id).SingleOrDefault();

                if (existsTran != null)
                {
                    existsTran.update_person = tranPhoto.update_person;
                    existsTran.update_datetime = tranPhoto.update_datetime;

                    existsTran.name = tranPhoto.name;
                    //existsTran.producer = tranPhoto.producer;
                    existsTran.short_description = tranPhoto.short_description;
                    //existsTran.description = tranPhoto.description;

                    existsTran.status = tranPhoto.status;

                    db.Entry(existsTran).State = EntityState.Modified;

                    db.SaveChanges();
                }
                else
                {
                    db.TBL_Tran_Photos.Add(tranPhoto);
                    db.SaveChanges();
                }

                using (db)
                {
                    var photos = from g in db.TBL_Tran_Photos
                                join lang in db.TBL_Languages
                                on g.language_id equals lang.row_id
                                where g.photo_row_id == tranPhoto.photo_row_id
                                select new
                                {
                                    g.row_id
                                    ,
                                    lang.language_name
                                    ,
                                    g.language_id
                                    ,
                                    g.name
                                    ,
                                    g.status
                                };




                    return this.Json(photos.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Tran Photo", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult DeleteTranPhoto(int photoid, int langId = 0)
        {
            try
            {

                using (db)
                {
                    List<TBL_Tran_Photo> tranPhotos = (from g in db.TBL_Tran_Photos
                                                     where g.photo_row_id == photoid && g.language_id == langId
                                                     select g).ToList();

                    foreach (TBL_Tran_Photo tranPhoto in tranPhotos)
                        db.TBL_Tran_Photos.Remove(tranPhoto);

                    db.SaveChanges();


                    var photos = from g in db.TBL_Tran_Photos
                                join lang in db.TBL_Languages
                                on g.language_id equals lang.row_id
                                where g.photo_row_id == photoid
                                select new
                                {
                                    g.row_id
                                    ,
                                    lang.language_name
                                    ,
                                    g.language_id
                                    ,
                                    g.name
                                    ,
                                    g.status
                                };




                    return this.Json(photos.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Delete Tran Photo", exception);
                return View("Error");
            }
        }


    }
}
