﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using log4net;
using WAP_CMS.Models;
using WAP_CMS.Common;

namespace WAP_CMS.Controllers
{
    [AccountAuthorization]
    public class PolicyController : Controller
    {
        
        private CMSDbContext db = new CMSDbContext();

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //
        // GET: /Policy/
        public ActionResult Index()
        {
            var policy = db.TBL_Policies.OrderByDescending(p => p.update_datetime);
            return View(policy.ToList());
        }

        [AccountAuthorization]
        public ActionResult Edit(int id = 0)
        {
            try
            {
                TBL_POLICY policy = db.TBL_Policies.Find(id);
                if (policy != null)
                {
                    return View(policy);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception exception)
            {
                log.Error("Policy/Edit", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Edit(TBL_POLICY policy)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    policy.update_datetime = DateTime.Now;
                    if (policy.register_datetime == null)
                    {
                        policy.register_datetime = DateTime.Now;
                    }
                    db.Entry(policy).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["MESSAGE"] = "Policy is saved successfully";
                }
                catch (Exception exception)
                {
                    log.Error("Policy/Edit", exception);
                    return View("Error");
                }
            }
            return View();

        }

    }
}
