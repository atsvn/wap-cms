﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using log4net;
using WAP_CMS.Models;
using System.Web.Script.Serialization;

namespace WAP_CMS.Controllers
{
    public class VideoCategoryController : Controller
    {
        private CMSDbContext db = new CMSDbContext();

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [AccountAuthorization]
        public ActionResult Index()
        {
            try
            {
                //var categories = db.TBL_Video_Categorys.Where(c => c.status != 9).OrderBy(c => c.category_name);
                using (db)
                {
                    var categories = from cat in db.TBL_Video_Categorys
                                     where cat.status != 9
                                     select new
                                     {
                                         cat,
                                         total = (from r in db.TBL_Category_Videos where r.video_category_row_id == cat.row_id select r).Count()
                                     };

                    List<TBL_Video_Category> lst = new List<TBL_Video_Category>();

                    foreach (var category in categories)
                    {
                        category.cat.totalVideos = category.total;

                        lst.Add(category.cat);
                    }

                    return View("Index", lst);
                }

            }
            catch (Exception exception)
            {
                log.Error("Video/Category", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        public ActionResult Create()
        {
            return View();
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult Create(TBL_Video_Category category)
        {
            try
            {
                TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);

                category.register_person = registerPerson.row_id;
                category.update_person = registerPerson.row_id;
                category.register_datetime = DateTime.Now;
                category.update_datetime = DateTime.Now;

                if (ModelState.IsValid)
                {
                    int checkVideo = db.TBL_Video_Categorys.Where(c => c.category_name.Equals(category.category_name)).Count();

                    if (checkVideo > 0)
                    {
                        ModelState.AddModelError("category_name", "Category name is already exists! Please choose another");

                        return View(category);
                    }
                    else
                    {
                        db.TBL_Video_Categorys.Add(category);
                        db.SaveChanges();

                        TempData["MESSAGE"] = "Category is created successfully";
                        return RedirectToAction("Edit", new { id = category.row_id });
                    }
                }

                return View(category);
            }
            catch (Exception exception)
            {
                log.Error("Video Category/Create", exception);
                return View("Error");
            }

        }


        [AccountAuthorization]
        public ActionResult Edit(int id = 0)
        {
            try
            {
                ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                TBL_Video_Category category = db.TBL_Video_Categorys.Find(id);

                if (category != null)
                {
                    using (db)
                    {
                        var VideoCategoriesTran = from cat in db.TBL_Tran_Video_Categorys
                                                 join lang in db.TBL_Languages
                                                 on cat.language_id equals lang.row_id
                                                 where cat.category_row_id == category.row_id
                                                 select new
                                                 {
                                                     cat
                                                     ,
                                                     lang.language_name

                                                 };

                        List<TBL_Tran_Video_Category> VideoCats = new List<TBL_Tran_Video_Category>();

                        foreach (var VideoCat in VideoCategoriesTran)
                        {
                            VideoCat.cat.language_name = VideoCat.language_name;

                            VideoCats.Add(VideoCat.cat);
                        }


                        ViewBag.TranVideoCategories = VideoCats;
                    }

                    return View(category);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Video Category/Edit/id", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult Edit(TBL_Video_Category category)
        {
            try
            {
                using (db)
                {
                    ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                    var VideoCategoriesTran = from cat in db.TBL_Tran_Video_Categorys
                                             join lang in db.TBL_Languages
                                             on cat.language_id equals lang.row_id
                                             where cat.category_row_id == category.row_id
                                             select new
                                             {
                                                 cat
                                                 ,
                                                 lang.language_name

                                             };

                    List<TBL_Tran_Video_Category> VideoCats = new List<TBL_Tran_Video_Category>();

                    foreach (var VideoCat in VideoCategoriesTran)
                    {
                        VideoCat.cat.language_name = VideoCat.language_name;

                        VideoCats.Add(VideoCat.cat);
                    }


                    ViewBag.TranVideoCategories = VideoCats;


                    TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);

                    category.update_person = registerPerson.row_id;
                    category.update_datetime = DateTime.Now;

                    if (ModelState.IsValid)
                    {
                        int checkVideo = db.TBL_Video_Categorys.Where(c => c.category_name.Equals(category.category_name) && c.row_id != category.row_id).Count();

                        if (checkVideo > 0)
                        {
                            ModelState.AddModelError("category_name", "Category name is already exists! Please choose another");

                            return View(category);
                        }
                        else
                        {
                            db.Entry(category).State = EntityState.Modified;
                            db.SaveChanges();

                            TempData["MESSAGE"] = "Category is saved successfully";
                            return View(category);
                        }
                    }
                }

                return View(category);
            }
            catch (Exception exception)
            {
                log.Error("Video Category/Edit", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpGet]
        public ActionResult TranVideoCategory(int catid, int lang = 0)
        {
            try
            {
                if (catid > 0 && lang > 0)
                {
                    TBL_Tran_Video_Category tranVideoCategory = db.TBL_Tran_Video_Categorys.Where(g => g.language_id == lang && g.category_row_id == catid).SingleOrDefault();

                    if (tranVideoCategory != null)
                    {
                        return PartialView("TranVideoCategory", tranVideoCategory);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("Tran Video Category", exception);
                return View("Error");
            }
            return PartialView();
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult TranVideoCategory(TBL_Tran_Video_Category tranVideoCategory)
        {
            try
            {
                TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);


                tranVideoCategory.update_person = registerPerson.row_id;

                tranVideoCategory.update_datetime = DateTime.Now;

                if (tranVideoCategory.row_id == 0)
                {
                    tranVideoCategory.register_person = registerPerson.row_id;
                    tranVideoCategory.register_datetime = DateTime.Now;
                }

                //if (ModelState.IsValid)
                //{

                TBL_Tran_Video_Category existsTran = db.TBL_Tran_Video_Categorys.Where(t => t.language_id == tranVideoCategory.language_id && t.category_row_id == tranVideoCategory.category_row_id).SingleOrDefault();

                if (existsTran != null)
                {
                    existsTran.update_person = tranVideoCategory.update_person;
                    existsTran.update_datetime = tranVideoCategory.update_datetime;
                    existsTran.category_name = tranVideoCategory.category_name;
                    existsTran.status = tranVideoCategory.status;

                    db.Entry(existsTran).State = EntityState.Modified;

                    db.SaveChanges();
                }
                else
                {
                    db.TBL_Tran_Video_Categorys.Add(tranVideoCategory);
                    db.SaveChanges();
                }

                using (db)
                {
                    var VideoCategoriesTran = from cat in db.TBL_Tran_Video_Categorys
                                             join lang in db.TBL_Languages
                                             on cat.language_id equals lang.row_id
                                             where cat.category_row_id == tranVideoCategory.category_row_id
                                             select new
                                             {
                                                 cat.row_id
                                                 ,
                                                 lang.language_name
                                                 ,
                                                 cat.language_id
                                                 ,
                                                 cat.category_name
                                                 ,
                                                 cat.status
                                             };




                    return this.Json(VideoCategoriesTran.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Tran Video Category", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult DeleteTranVideoCategory(int catid, int langId = 0)
        {
            try
            {

                using (db)
                {
                    List<TBL_Tran_Video_Category> categories = (from cat in db.TBL_Tran_Video_Categorys
                                                               where cat.category_row_id == catid && cat.language_id == langId
                                                               select cat).ToList();

                    foreach (TBL_Tran_Video_Category tranVideoCat in categories)
                        db.TBL_Tran_Video_Categorys.Remove(tranVideoCat);

                    db.SaveChanges();


                    var VideoCategoriesTran = from cat in db.TBL_Tran_Video_Categorys
                                             join lang in db.TBL_Languages
                                             on cat.language_id equals lang.row_id
                                             where cat.category_row_id == catid
                                             select new
                                             {
                                                 cat.row_id
                                                 ,
                                                 lang.language_name
                                                 ,
                                                 cat.language_id
                                                 ,
                                                 cat.category_name
                                                 ,
                                                 cat.status
                                             };




                    return this.Json(VideoCategoriesTran.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Tran Video Category", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        public ActionResult Detail(int id = 0)
        {
            try
            {
                ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                TBL_Video_Category category = db.TBL_Video_Categorys.Find(id);

                if (category != null)
                {
                    using (db)
                    {
                        var VideoCategoriesTran = from cat in db.TBL_Tran_Video_Categorys
                                                 join lang in db.TBL_Languages
                                                 on cat.language_id equals lang.row_id
                                                 where cat.category_row_id == category.row_id
                                                 select new
                                                 {
                                                     cat
                                                     ,
                                                     lang.language_name

                                                 };

                        List<TBL_Tran_Video_Category> VideoCats = new List<TBL_Tran_Video_Category>();

                        foreach (var VideoCat in VideoCategoriesTran)
                        {
                            VideoCat.cat.language_name = VideoCat.language_name;

                            VideoCats.Add(VideoCat.cat);
                        }


                        ViewBag.TranVideoCategories = VideoCats;
                    }

                    return View(category);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Video Category/Detail/id", exception);
                return View("Error");
            }
        }


        [AccountAuthorization]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                // Check if exists Video in category
                int checkExists = db.TBL_Category_Videos.Where(c => c.video_category_row_id == id).Count();

                if (checkExists > 0)
                {
                    TBL_Video_Category VideoCategory = db.TBL_Video_Categorys.Where(g => g.row_id == id).SingleOrDefault();

                    TBL_Account updatePerson = ((TBL_Account)Session["_ADMIN__"]);

                    if (VideoCategory != null)
                    {
                        VideoCategory.update_person = updatePerson.row_id;
                        VideoCategory.update_datetime = DateTime.Now;

                        VideoCategory.status = 9;

                        db.Entry(VideoCategory).State = EntityState.Modified;

                        db.SaveChanges();
                    }
                }
                else
                {
                    // Physical remove from db
                    // First remove Video translate
                    using (db)
                    {
                        var tranVideoCategories = from t in db.TBL_Tran_Video_Categorys
                                                 where t.category_row_id == id
                                                 select t;

                        foreach (TBL_Tran_Video_Category tranCat in tranVideoCategories)
                            db.Entry(tranCat).State = EntityState.Deleted;

                        db.SaveChanges();


                        var VideoCategories = from t in db.TBL_Video_Categorys
                                             where t.row_id == id
                                             select t;

                        foreach (TBL_Video_Category VideoCat in VideoCategories)
                            db.Entry(VideoCat).State = EntityState.Deleted;

                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Video Category/Delete/id", exception);
                return View("Error");
            }
        }
    }
}