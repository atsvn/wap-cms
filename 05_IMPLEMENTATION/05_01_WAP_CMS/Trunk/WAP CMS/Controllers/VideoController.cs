﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using log4net;
using WAP_CMS.Models;
using WAP_CMS.Common;

namespace WAP_CMS.Controllers
{
    public class VideoController : Controller
    {
        private CMSDbContext db = new CMSDbContext();

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Dictionary<string, HttpPostedFileBase> postedFileList = new Dictionary<string, HttpPostedFileBase>();

        [AccountAuthorization]
        public ActionResult Index(int catId = -1)
        {
            try
            {
                var categories = db.TBL_Video_Categorys.Where(c => c.status != 9);
                ViewBag.categories = categories.ToList();
                ViewBag.catId = catId;
                ViewBag.VideoCategory = new SelectList(categories.ToList(), "row_id", "category_name");
                List<TBL_Video> vList = new List<TBL_Video>();
                if(catId == -1){
                    vList = db.TBL_Videos.Where(g => g.status != 9).OrderByDescending(g => g.update_datetime).ToList();
                }else{
                    var videosWithCatFilter = from v in db.TBL_Videos
                                              join c in db.TBL_Category_Videos
                                              on v.row_id equals c.video_row_id
                                              where v.status != 9 && c.video_category_row_id == catId
                                              orderby v.update_datetime descending
                                              select new
                                              {
                                                  v
                                              };
                    foreach (var vid in videosWithCatFilter)
                    {
                        vList.Add(vid.v);
                    }
                }
                
                
                var videosList = new List<TBL_Video>();
                using (db)
                {
                    foreach (var video in vList)
                    {
                        video.categories = new List<TBL_Video_Category>();
                        var videoCategorys = from c in db.TBL_Video_Categorys
                                             join l in db.TBL_Category_Videos
                                             on c.row_id equals l.video_category_row_id
                                             where l.video_row_id == video.row_id && c.status != 9
                                             select new
                                             {
                                                 c
                                             };

                        foreach (var category in videoCategorys)
                        {
                            video.categories.Add(category.c);
                        }
                        videosList.Add(video);
                    }
                }
                
                return View("Index", videosList);
            }
            catch (Exception exception)
            {
                log.Error("Video/Index", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        public ActionResult Create()
        {
            return View();
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Create(TBL_Video video)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    bool isError = false;
                    HttpFileCollectionBase postedFiles = Request.Files;

                    for (int i = 0; i < postedFiles.Count; i++)
                    {
                        HttpPostedFileBase file = postedFiles[i];
                        string fileName = file.FileName.ToLower();


                        switch (postedFiles.GetKey(i))
                        {
                            case "Image":
                                if (fileName.Length > 0)
                                {
                                    if (file.ContentType.Contains("image"))
                                    {
                                        postedFileList.Add("Image", file);
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("ImageURL", "The Image field must be image.");
                                        isError = true;
                                    }
                                }
                                else
                                {
                                    postedFileList.Remove("Image");
                                }

                                break;
                            case "videourl":
                                if (fileName.Length > 0)
                                {
                                    if (file.ContentType.Contains("3gpp"))
                                    {
                                        postedFileList.Add("videourl", file);
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("videourl", "The Video field must be 3GP file.");
                                        isError = true;
                                    }
                                }
                                else
                                {
                                    postedFileList.Remove("videourl");
                                }

                                break;
                            default:
                                break;
                        }
                    }

                    // File upload error
                    if (isError)
                    {
                        return View(video);
                    }
                    else
                    {
                        foreach (var item in postedFileList)
                        {
                            HttpPostedFileBase file = item.Value;

                            switch (item.Key)
                            {
                                case "Image":
                                    video.img = Util.FtpUploadVideoImg((new WebImage(file.InputStream)).Resize(Convert.ToInt32(ConfigurationManager.AppSettings["ImageWidth"]), Convert.ToInt32(ConfigurationManager.AppSettings["ImageHeight"]), true, true));

                                    break;
                                case "videourl":
                                    video.videosize = file.ContentLength;
                                    video.videourl = Util.FtpUploadVideo(file);

                                    break;


                            }
                        }

                        TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);

                        video.register_person = registerPerson.row_id;
                        video.update_person = registerPerson.row_id;

                        video.register_datetime = DateTime.Now;
                        video.update_datetime = DateTime.Now;

                        db.TBL_Videos.Add(video);
                        db.SaveChanges();

                        TempData["MESSAGE"] = "Your video is created successfully";
                        return RedirectToAction("Edit", new { id = video.row_id });
                    }


                }
                catch (Exception exception)
                {
                    log.Error("Video/Create", exception);
                    return View("Error");
                }
            }

            return View(video);
        }

        [AccountAuthorization]
        public ActionResult Edit(int id = 0)
        {
            try
            {
                TBL_Video video = db.TBL_Videos.Find(id);

                if (video != null)
                {
                    ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                    using (db)
                    {
                        var videoTrans = from g in db.TBL_Tran_Videos
                                         join lang in db.TBL_Languages
                                         on g.language_id equals lang.row_id
                                         where g.video_row_id == video.row_id
                                         select new
                                         {
                                             g
                                             ,
                                             lang.language_name

                                         };

                        List<TBL_Tran_Video> videos = new List<TBL_Tran_Video>();

                        foreach (var videoTran in videoTrans)
                        {
                            videoTran.g.language_name = videoTran.language_name;

                            videos.Add(videoTran.g);
                        }


                        ViewBag.TranVideos = videos;


                        video.allCategories = db.TBL_Video_Categorys.Where(c => c.status != 9).ToList();

                        var videoCategorys = from c in db.TBL_Video_Categorys
                                             join l in db.TBL_Category_Videos
                                             on c.row_id equals l.video_category_row_id
                                             where l.video_row_id == video.row_id && c.status != 9
                                             select new
                                             {
                                                 c
                                             };

                        video.categories = new List<TBL_Video_Category>();
                        foreach (var category in videoCategorys)
                        {
                            video.categories.Add(category.c);
                        }
                    }

                    return View(video);
                }
                else
                    return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Video/Edit/id", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Edit(TBL_Video video)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (db)
                    {
                        ViewBag.languages = new SelectList(db.TBL_Languages.ToList(), "row_id", "language_name");

                        var videoTrans = from g in db.TBL_Tran_Videos
                                         join lang in db.TBL_Languages
                                         on g.language_id equals lang.row_id
                                         where g.video_row_id == video.row_id
                                         select new
                                         {
                                             g
                                             ,
                                             lang.language_name

                                         };

                        List<TBL_Tran_Video> videos = new List<TBL_Tran_Video>();

                        foreach (var videoTran in videoTrans)
                        {
                            videoTran.g.language_name = videoTran.language_name;

                            videos.Add(videoTran.g);
                        }


                        ViewBag.TranVideos = videos;


                        bool isError = false;
                        HttpFileCollectionBase postedFiles = Request.Files;

                        for (int i = 0; i < postedFiles.Count; i++)
                        {
                            HttpPostedFileBase file = postedFiles[i];
                            string fileName = file.FileName.ToLower();


                            switch (postedFiles.GetKey(i))
                            {
                                case "Image":
                                    if (fileName.Length > 0)
                                    {
                                        if (file.ContentType.Contains("image"))
                                        {
                                            postedFileList.Add("Image", file);
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("ImageURL", "The Image field must be image.");
                                            isError = true;
                                        }
                                    }
                                    else
                                    {
                                        postedFileList.Remove("Image");
                                    }

                                    break;
                                case "videourl":
                                    if (fileName.Length > 0)
                                    {
                                        if (file.ContentType.Contains("video"))
                                        {
                                            postedFileList.Add("videourl", file);
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("videourl", "The video field must be image.");
                                            isError = true;
                                        }
                                    }
                                    else
                                    {
                                        postedFileList.Remove("videourl");
                                    }

                                    break;


                                default:
                                    break;
                            }
                        }

                        // File upload error
                        if (isError)
                        {
                            video.allCategories = db.TBL_Video_Categorys.Where(c => c.status != 9).ToList();

                            var videoCategorys = from c in db.TBL_Video_Categorys
                                                 join l in db.TBL_Category_Videos
                                                 on c.row_id equals l.video_category_row_id
                                                 where l.video_row_id == video.row_id && c.status != 9
                                                 select new
                                                 {
                                                     c
                                                 };

                            video.categories = new List<TBL_Video_Category>();
                            foreach (var category in videoCategorys)
                            {
                                video.categories.Add(category.c);
                            }

                            return View(video);
                        }
                        else
                        {
                            foreach (var item in postedFileList)
                            {
                                HttpPostedFileBase file = item.Value;

                                switch (item.Key)
                                {
                                    case "Image":
                                        video.img = Util.FtpUploadVideoImg((new WebImage(file.InputStream)).Resize(Convert.ToInt32(ConfigurationManager.AppSettings["ImageWidth"]), Convert.ToInt32(ConfigurationManager.AppSettings["ImageHeight"]), true, true));

                                        break;
                                    case "videourl":
                                        video.videosize = file.ContentLength;
                                        video.videourl = Util.FtpUploadVideo(file);

                                        break;
                                }
                            }

                            TBL_Account updatePerson = ((TBL_Account)Session["_ADMIN__"]);

                            video.update_person = updatePerson.row_id;
                            video.update_datetime = DateTime.Now;

                            db.Entry(video).State = EntityState.Modified;
                            db.SaveChanges();

                            // Remove old game category
                            List<TBL_Category_Video> categoryVideos = (from c in db.TBL_Category_Videos
                                                                       where c.video_row_id == video.row_id
                                                                       select c).ToList();

                            if (categoryVideos.Count > 0)
                            {
                                foreach (TBL_Category_Video ob in categoryVideos)
                                    db.TBL_Category_Videos.Remove(ob);

                                db.SaveChanges();
                            }

                            // Add news
                            if (video.PostedCategoryRowIds != null && video.PostedCategoryRowIds.Length > 0)
                            {
                                for (int i = 0; i < video.PostedCategoryRowIds.Length; i++)
                                {
                                    TBL_Category_Video cg = new TBL_Category_Video();
                                    cg.video_row_id = video.row_id;
                                    cg.video_category_row_id = Convert.ToInt32(video.PostedCategoryRowIds[i]);

                                    db.TBL_Category_Videos.Add(cg);
                                }
                                db.SaveChanges();
                            }


                            video.allCategories = db.TBL_Video_Categorys.Where(c => c.status != 9).ToList();

                            var videoCategorys = from c in db.TBL_Video_Categorys
                                                 join l in db.TBL_Category_Videos
                                                 on c.row_id equals l.video_category_row_id
                                                 where l.video_row_id == video.row_id && c.status != 9
                                                 select new
                                                 {
                                                     c
                                                 };

                            video.categories = new List<TBL_Video_Category>();
                            foreach (var category in videoCategorys)
                            {
                                video.categories.Add(category.c);
                            }


                            TempData["MESSAGE"] = "Video is saved successfully";
                            return View(video);
                        }
                    }
                }
                catch (Exception exception)
                {
                    log.Error("Video/Edit", exception);
                    return View("Error");
                }
            }

            return View(video);
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                TBL_Account deletePerson = ((TBL_Account)Session["_ADMIN__"]);

                TBL_Video video = db.TBL_Videos.Find(id);

                if (video != null)
                {
                    video.update_person = deletePerson.row_id;
                    video.update_datetime = DateTime.Now;

                    video.status = 9;

                    db.Entry(video).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                log.Error("Video/Delete", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        [HttpGet]
        public ActionResult TranVideo(int videoid, int lang = 0)
        {
            try
            {
                if (videoid > 0 && lang > 0)
                {
                    TBL_Tran_Video tranVideo = db.TBL_Tran_Videos.Where(g => g.language_id == lang && g.video_row_id == videoid).SingleOrDefault();

                    if (tranVideo != null)
                    {
                        return PartialView("TranVideo", tranVideo);
                    }
                }
            }
            catch (Exception exception)
            {
                log.Error("Tran Video", exception);
                return View("Error");
            }
            return PartialView();
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult TranVideo(TBL_Tran_Video tranVideo)
        {
            try
            {
                TBL_Account registerPerson = ((TBL_Account)Session["_ADMIN__"]);


                tranVideo.update_person = registerPerson.row_id;

                tranVideo.update_datetime = DateTime.Now;

                if (tranVideo.row_id == 0)
                {
                    tranVideo.register_person = registerPerson.row_id;
                    tranVideo.register_datetime = DateTime.Now;
                }

                //if (ModelState.IsValid)
                //{

                TBL_Tran_Video existsTran = db.TBL_Tran_Videos.Where(t => t.language_id == tranVideo.language_id && t.video_row_id == tranVideo.video_row_id).SingleOrDefault();

                if (existsTran != null)
                {
                    existsTran.update_person = tranVideo.update_person;
                    existsTran.update_datetime = tranVideo.update_datetime;

                    existsTran.name = tranVideo.name;
                    existsTran.short_description = tranVideo.short_description;
                    existsTran.description = tranVideo.description;

                    existsTran.status = tranVideo.status;

                    db.Entry(existsTran).State = EntityState.Modified;

                    db.SaveChanges();
                }
                else
                {
                    db.TBL_Tran_Videos.Add(tranVideo);
                    db.SaveChanges();
                }

                using (db)
                {
                    var videos = from g in db.TBL_Tran_Videos
                                 join lang in db.TBL_Languages
                                 on g.language_id equals lang.row_id
                                 where g.video_row_id == tranVideo.video_row_id
                                 select new
                                 {
                                     g.row_id
                                     ,
                                     lang.language_name
                                     ,
                                     g.language_id
                                     ,
                                     g.name
                                     ,
                                     g.status
                                 };




                    return this.Json(videos.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Tran Video", exception);
                return View("Error");
            }
        }

        [AccountAuthorization]
        [HttpPost]
        public ActionResult DeleteTranVideo(int videoid, int langId = 0)
        {
            try
            {

                using (db)
                {
                    List<TBL_Tran_Video> tranVideos = (from g in db.TBL_Tran_Videos
                                                       where g.video_row_id == videoid && g.language_id == langId
                                                       select g).ToList();

                    foreach (TBL_Tran_Video tranVideo in tranVideos)
                        db.TBL_Tran_Videos.Remove(tranVideo);

                    db.SaveChanges();


                    var videos = from g in db.TBL_Tran_Videos
                                 join lang in db.TBL_Languages
                                 on g.language_id equals lang.row_id
                                 where g.video_row_id == videoid
                                 select new
                                 {
                                     g.row_id
                                     ,
                                     lang.language_name
                                     ,
                                     g.language_id
                                     ,
                                     g.name
                                     ,
                                     g.status
                                 };




                    return this.Json(videos.ToList());
                }

                //}
            }
            catch (Exception exception)
            {
                log.Error("Delete Tran Video", exception);
                return View("Error");
            }
        }
    }



}