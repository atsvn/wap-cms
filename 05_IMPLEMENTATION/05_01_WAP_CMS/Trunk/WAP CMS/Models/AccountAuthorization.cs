﻿using System.Web;
using System.Web.Mvc;

namespace WAP_CMS.Models
{
    public class AccountAuthorization : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            HttpSessionStateBase session = httpContext.Session;
            return session["_ADMIN__"] != null;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            UrlHelper urlHelper = new UrlHelper(filterContext.RequestContext);
            filterContext.Result = new RedirectResult(urlHelper.Action("Signin", "Account"));
        }
    }
}