﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace WAP_CMS.Models
{
    public class CMSDbContext : DbContext
    {
        public DbSet<TBL_Account> TBL_Accounts { get; set; }
        
        public DbSet<TBL_Game> TBL_Games { get; set; }
        public DbSet<TBL_Game_Category> TBL_Game_Categorys { get; set; }
        public DbSet<TBL_Category_Game> TBL_Category_Games { get; set; }
        public DbSet<TBL_Tran_Game_Category> TBL_Tran_Game_Categorys { get; set; }
        public DbSet<TBL_Tran_Game> TBL_Tran_Games { get; set; }

        public DbSet<TBL_Video> TBL_Videos { get; set; }
        public DbSet<TBL_Video_Category> TBL_Video_Categorys { get; set; }
        public DbSet<TBL_Category_Video> TBL_Category_Videos { get; set; }
        public DbSet<TBL_Tran_Video_Category> TBL_Tran_Video_Categorys { get; set; }
        public DbSet<TBL_Tran_Video> TBL_Tran_Videos { get; set; }

        public DbSet<TBL_Photo> TBL_Photos { get; set; }
        public DbSet<TBL_Photo_Category> TBL_Photo_Categorys { get; set; }
        public DbSet<TBL_Category_Photo> TBL_Category_Photos { get; set; }
        public DbSet<TBL_Tran_Photo_Category> TBL_Tran_Photo_Categorys { get; set; }
        public DbSet<TBL_Tran_Photo> TBL_Tran_Photos { get; set; }

        public DbSet<TBL_Music> TBL_Musics { get; set; }
        public DbSet<TBL_Music_Category> TBL_Music_Categorys { get; set; }
        public DbSet<TBL_Category_Music> TBL_Category_Musics { get; set; }
        public DbSet<TBL_Tran_Music_Category> TBL_Tran_Music_Categorys { get; set; }
        public DbSet<TBL_Tran_Music> TBL_Tran_Musics { get; set; }

        public DbSet<TBL_POLICY> TBL_Policies { get; set; }

        public DbSet<TBL_Language> TBL_Languages { get; set; }

        public CMSDbContext()
            : base("CMSDb") 
        { 
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}