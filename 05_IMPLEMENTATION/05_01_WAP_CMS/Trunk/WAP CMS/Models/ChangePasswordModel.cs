﻿using System;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.Mvc;

namespace WAP_CMS.Models
{
    public class ChangePasswordModel
    {
        [Display(Name = "Current password")]
        [Required]
        public string CurrentPassword { get; set; }

        [Display(Name = "New password")]
        [Required]
        public string NewPassword { get; set; }

        [Display(Name = "Confirm password")]
        [Required]
        [Compare("NewPassword", ErrorMessage = "New password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}