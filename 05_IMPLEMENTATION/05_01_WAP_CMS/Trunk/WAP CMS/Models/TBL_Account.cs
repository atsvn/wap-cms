﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.Mvc;

namespace WAP_CMS.Models
{
    public class TBL_Account
    {
        [Key]
        public int row_id { get; set; }

        public int register_person { get; set; }
        public DateTime register_datetime { get; set; }

        public int update_person { get; set; }
        public DateTime update_datetime { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string username { get; set; }

        [Required]
        [Display(Name = "Password")]
        public string password { get; set; }

        public string email { get; set; }

        public DateTime? last_access { get; set; }

        public int status { get; set; }

    }
}