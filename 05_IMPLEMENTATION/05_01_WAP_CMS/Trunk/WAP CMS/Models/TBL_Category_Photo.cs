﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.Mvc;

namespace WAP_CMS.Models
{
    public class TBL_Category_Photo
    {
        [Key]
        public int row_id { get; set; }

        public int photo_category_row_id { get; set; }

        public int photo_row_id { get; set; }
    }
}