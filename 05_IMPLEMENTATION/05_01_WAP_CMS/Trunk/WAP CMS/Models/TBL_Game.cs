﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.Mvc;
using System.Configuration;

namespace WAP_CMS.Models
{
    public class TBL_Game
    {
        [Key]
        public int row_id { get; set; }

        public int register_person { get; set; }
        public DateTime register_datetime { get; set; }

        public int update_person { get; set; }
        public DateTime update_datetime { get; set; }

        public int status { get; set; }

        [NotMapped]
        [Display(Name = "Enable/Disable")]
        public bool Enable
        {
            get { return status == 1; }
            set
            {
                if (value)
                    status = 1;
                else
                    status = 0;
            }
        }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }


        [Display(Name = "Short description")]
        public string short_description { get; set; }

        [AllowHtml]
        [Display(Name = "Full description")]
        public string description { get; set; }


        [Display(Name = "Image")]
        public string img { get; set; }

        [NotMapped]
        public string ImgPath
        {
            get { return ConfigurationManager.AppSettings["ftpUrl"] + "/" + img; }
        }

        [Display(Name = "View")]
        public int view { get; set; }

        [Display(Name = "Download")]
        public int download { get; set; }

        [Display(Name = "Producer")]
        public string producer { get; set; }

        [Display(Name = "Android Game (.apk)")]
        public string androidappurl { get; set; }
        public double? androidappsize { get; set; }

        [NotMapped]
        public string AndroidappurlPath 
        { 
            get{ return ConfigurationManager.AppSettings["ftpUrl"] + "/" + androidappurl;}
        }

        [Display(Name = "Java Game (.jad, .jar)")]
        public string javaappurl { get; set; }
        public double? javaappsize { get; set; }

        [NotMapped]
        public string JavaappurlPath 
        { 
            get{ return ConfigurationManager.AppSettings["ftpUrl"] + "/" + javaappurl;}
        }
        

        [Display(Name = "iOS Game (.ipa)")]
        public string iosappurl { get; set; }
        public double? iosappsize { get; set; }

        [NotMapped]
        public string IosappurlPath 
        {
            get { return ConfigurationManager.AppSettings["ftpUrl"] + "/" + iosappurl; }
        }


        public List<TBL_Game_Category> allCategories;

        public List<TBL_Game_Category> categories;

        public string[] PostedCategoryRowIds { get; set; }
    }
}