﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.Mvc;

namespace WAP_CMS.Models
{
    public class TBL_Language
    {
        [Key]
        public int row_id { get; set; }

        public string language_name { get; set; }

        public string language_code { get; set; }
    }
}