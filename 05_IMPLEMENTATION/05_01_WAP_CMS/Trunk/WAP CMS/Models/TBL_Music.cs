﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.Mvc;
using System.Configuration;

namespace WAP_CMS.Models
{
    public class TBL_Music
    {
        [Key]
        public int row_id { get; set; }

        public int register_person { get; set; }
        public DateTime register_datetime { get; set; }

        public int update_person { get; set; }
        public DateTime update_datetime { get; set; }

        public int status { get; set; }

        [NotMapped]
        [Display(Name = "Enable/Disable")]
        public bool Enable
        {
            get { return status == 1; }
            set
            {
                if (value)
                    status = 1;
                else
                    status = 0;
            }
        }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }


        [Display(Name = "Short description")]
        public string short_description { get; set; }

        [Display(Name = "View")]
        public int view { get; set; }

        [Display(Name = "Download")]
        public int download { get; set; }

        [Display(Name = "music_file")]
        public string music_file { get; set; }

        [NotMapped]
        public string music_filePath
        {
            get { return ConfigurationManager.AppSettings["ftpUrl"] + "/" + music_file; }
        }

        [Display(Name = "amr_file")]
        public string amr_file { get; set; }

        [NotMapped]
        public string amr_filePath
        {
            get { return ConfigurationManager.AppSettings["ftpUrl"] + "/" + amr_file; }
        }

        [Display(Name = "mmf_file")]
        public string mmf_file { get; set; }

        [NotMapped]
        public string mmf_filePath
        {
            get { return ConfigurationManager.AppSettings["ftpUrl"] + "/" + mmf_file; }
        }

        public List<TBL_Music_Category> allCategories;

        public List<TBL_Music_Category> categories;

        public string[] PostedCategoryRowIds { get; set; }
    }
}