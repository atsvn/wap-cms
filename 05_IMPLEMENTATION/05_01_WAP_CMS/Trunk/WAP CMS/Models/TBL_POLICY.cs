﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.Mvc;
using System.Configuration;


namespace WAP_CMS.Models
{
    public class TBL_POLICY
    {
        [Key]
        public int row_id { get; set; }

        
        public DateTime register_datetime { get; set; }

        
        public DateTime update_datetime { get; set; }

        public int type { get; set; }

        public string content { get; set; }

        [NotMapped]
        [Display(Name = "Policy type")]
        public string typeString
        {
            get
            {
                switch (type)
                {
                    case 1:
                        return "Liên hệ";
                    case 2:
                        return "Luật chơi và Giải thưởng";
                    case 3:
                        return "Danh sách người trúng thưởng";
                    default:
                        return "Undefined content !";
                }
            }
            set
            {
                
            }
        }

    }
}