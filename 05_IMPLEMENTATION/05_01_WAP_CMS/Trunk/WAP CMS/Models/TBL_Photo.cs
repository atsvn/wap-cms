﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.Mvc;
using System.Configuration;

namespace WAP_CMS.Models
{
    public class TBL_Photo
    {
        [Key]
        public int row_id { get; set; }

        public int register_person { get; set; }
        public DateTime register_datetime { get; set; }

        public int update_person { get; set; }
        public DateTime update_datetime { get; set; }

        public int status { get; set; }

        [NotMapped]
        [Display(Name = "Enable/Disable")]
        public bool Enable
        {
            get { return status == 1; }
            set
            {
                if (value)
                    status = 1;
                else
                    status = 0;
            }
        }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }


        [Display(Name = "Short description")]
        public string short_description { get; set; }

        [Display(Name = "Image")]
        public string img { get; set; }

        [NotMapped]
        public string ImgPath
        {
            get { return ConfigurationManager.AppSettings["ftpUrl"] + "/" + img; }
        }

        [Display(Name = "Image-ota")]
        public string img_ota { get; set; }

        [NotMapped]
        public string ImgOtaPath
        {
            get { return ConfigurationManager.AppSettings["ftpUrl"] + "/" + img_ota; }
        }

        [Display(Name = "View")]
        public int view { get; set; }

        [Display(Name = "Download")]
        public int download { get; set; }

        public List<TBL_Photo_Category> allCategories;

        public List<TBL_Photo_Category> categories;

        public string[] PostedCategoryRowIds { get; set; }

    }
}