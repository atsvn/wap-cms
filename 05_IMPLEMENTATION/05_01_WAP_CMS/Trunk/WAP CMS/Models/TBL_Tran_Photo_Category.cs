﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.Mvc;

namespace WAP_CMS.Models
{
    public class TBL_Tran_Photo_Category
    {
        [Key]
        public int row_id { get; set; }

        public int register_person { get; set; }
        public DateTime? register_datetime { get; set; }

        public int update_person { get; set; }
        public DateTime update_datetime { get; set; }

        public int status { get; set; }

        public int language_id { get; set; }
        public int category_row_id { get; set; }

        [NotMapped]
        [Display(Name = "Enable/Disable")]
        public bool Enable
        {
            get { return status == 1; }
            set
            {
                if (value)
                    status = 1;
                else
                    status = 0;
            }
        }

        [Required]
        [Display(Name = "Category name")]
        public string category_name { get; set; }

        public TBL_Tran_Photo_Category()
        {
            this.status = 1;
        }


        [NotMapped]
        public string language_name { get; set; }
    }
}