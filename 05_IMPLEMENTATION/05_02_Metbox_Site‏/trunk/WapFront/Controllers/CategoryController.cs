﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WapFront.Models;
using System.IO;
using System.Configuration;
using WapFront.Util;
namespace WapFront.Controllers
{
    public class CategoryController : Controller
    {

        private MediaDBContext db = new MediaDBContext();
        //
        // GET: /Category/

        public ActionResult ListCategory()
        {
            ViewBag.client = new ClientUtil(Request.UserAgent.ToLower()).clientId;
            return PartialView();
        }

        public ActionResult ShowCategory(int Id = 0)
        {
            ClientUtil client = new ClientUtil(Request.UserAgent.ToLower());
            ViewBag.Id = Id;
            int i, itemPerPage = 5;
            double n = db.Media.Where(m => m.CategoryId == Id).ToList().Count() / itemPerPage;
            int numberOfPage = (int)Math.Ceiling(n);
            int page = (int.TryParse(Request.Params["page"], out i)) ? int.Parse(Request.Params["page"]) : 1;
            ViewBag.Page = (page > numberOfPage) ? numberOfPage : page;
            ViewBag.CSS = client.css;
            ViewBag.Category = db.Categories.Where(c => c.Id == Id).SingleOrDefault().Name;
            return PartialView(db.Media.Where(m => m.CategoryId == Id).Where(m => m.Status == 1).OrderBy(m => m.CreatedDateTime).Skip(itemPerPage * (page - 1)).Take(itemPerPage).ToList());
        }


    }
}
