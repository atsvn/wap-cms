﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WapFront.Util;

namespace WapFront.Controllers
{
    public class GameController : Controller
    {
        //
        // GET: /Game/

        public ActionResult Index(int? id)
        {
            ClientUtil client = new ClientUtil(Request.UserAgent.ToLower());
            ViewBag.CSS = client.css;
            ViewBag.Title = "Trò chơi";
            ViewBag.CSSClass = "game";
            if (id.HasValue)
            {
                return View("Detail/Index" + client.client);
            }
            else
            {
                return View("Index" + client.client);
            }
            
        }

        public ActionResult Topic(int Id = 1)
        {
            ClientUtil client = new ClientUtil(Request.UserAgent.ToLower());
            ViewBag.CSS = client.css;
            ViewBag.Title = "Trò chơi";
            ViewBag.CSSClass = "game";
            return View("Index" + client.client);
        }

    }
}
