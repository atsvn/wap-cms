﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WapFront.Models;
using System.IO;
using System.Configuration;
using WapFront.Util;

namespace WapFront.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            ClientUtil client = new ClientUtil(Request.UserAgent.ToLower());
            ViewBag.CSS = client.css;
            ViewBag.CSSClass = "home";
            return View("Index" + client.client);    
            
        }

        public ActionResult Category()
        {
            string agent = Request.UserAgent.ToLower();
            string smartPhonePattern = "ipad|ipod|android|opera mini|palm os|palm|hiptop|avantgo|plucker|xiino|blazer|elaine|mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|wireless| mobi|ahong|lg380|lgku|lgu900|lg210|lg47|lg920|lg840|lg370|sam-r|mg50|s55|g83|t66|vx400|mk99|d615|d763|el370|sl900|mp500|samu3|samu4|vx10|xda_|samu5|samu6|samu7|samu9|a615|b832|m881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|p404i|s210|c5100|teleca|s940|c500|s590|foma|samsu|vx8|vx9|a1000|_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|s800|8325rc|ac831|mw200|brew |d88|htc/|htc_touch|355x|m50|km100|d736|p-9521|telco|sl74|ktouch|m4u/|me702|8325rc|kddi|phone|lg |sonyericsson|samsung|240x|x320|vx10|nokia|sony cmd|motorola|up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|pocket|kindle|mobile|psp|treo|iris|3g_t|windows ce|opera mobi|windows ce; smartphone;|windows ce; iemobile";
            string WAPPattern = "j2me|nokia109";

            ViewBag.Agent = agent;

            if (System.Text.RegularExpressions.Regex.IsMatch(agent, WAPPattern))
            {
                ViewBag.CSS = "~/Content/WAP.css";
                return View("Category-WAP");
            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(agent, smartPhonePattern))
            {
                ViewBag.CSS = "~/Content/SmartPhone.css";
                return View("Category-SmartPhone");
            }
            else
            {
                ViewBag.CSS = "~/Content/Desktop.css";
                return View();
            }
        }

        public ActionResult Detail()
        {
            string agent = Request.UserAgent.ToLower();
            string smartPhonePattern = "ipad|ipod|android|opera mini|palm os|palm|hiptop|avantgo|plucker|xiino|blazer|elaine|mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|wireless| mobi|ahong|lg380|lgku|lgu900|lg210|lg47|lg920|lg840|lg370|sam-r|mg50|s55|g83|t66|vx400|mk99|d615|d763|el370|sl900|mp500|samu3|samu4|vx10|xda_|samu5|samu6|samu7|samu9|a615|b832|m881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|p404i|s210|c5100|teleca|s940|c500|s590|foma|samsu|vx8|vx9|a1000|_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|s800|8325rc|ac831|mw200|brew |d88|htc/|htc_touch|355x|m50|km100|d736|p-9521|telco|sl74|ktouch|m4u/|me702|8325rc|kddi|phone|lg |sonyericsson|samsung|240x|x320|vx10|nokia|sony cmd|motorola|up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|pocket|kindle|mobile|psp|treo|iris|3g_t|windows ce|opera mobi|windows ce; smartphone;|windows ce; iemobile";
            string WAPPattern = "j2me|nokia109";

            ViewBag.Agent = agent;

            if (System.Text.RegularExpressions.Regex.IsMatch(agent, WAPPattern))
            {
                ViewBag.CSS = "~/Content/WAP.css";
                return View("Detail-WAP");
            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(agent, smartPhonePattern))
            {
                ViewBag.CSS = "~/Content/SmartPhone.css";
                return View("Detail-SmartPhone");
            }
            else
            {
                ViewBag.CSS = "~/Content/Desktop.css";
                return View();
            }
        }

        public ActionResult Search()
        {
            string agent = Request.UserAgent.ToLower();
            string smartPhonePattern = "ipad|ipod|android|opera mini|palm os|palm|hiptop|avantgo|plucker|xiino|blazer|elaine|mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|wireless| mobi|ahong|lg380|lgku|lgu900|lg210|lg47|lg920|lg840|lg370|sam-r|mg50|s55|g83|t66|vx400|mk99|d615|d763|el370|sl900|mp500|samu3|samu4|vx10|xda_|samu5|samu6|samu7|samu9|a615|b832|m881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|p404i|s210|c5100|teleca|s940|c500|s590|foma|samsu|vx8|vx9|a1000|_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|s800|8325rc|ac831|mw200|brew |d88|htc/|htc_touch|355x|m50|km100|d736|p-9521|telco|sl74|ktouch|m4u/|me702|8325rc|kddi|phone|lg |sonyericsson|samsung|240x|x320|vx10|nokia|sony cmd|motorola|up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|pocket|kindle|mobile|psp|treo|iris|3g_t|windows ce|opera mobi|windows ce; smartphone;|windows ce; iemobile";
            string WAPPattern = "j2me|nokia109";

            ViewBag.Agent = agent;

            if (System.Text.RegularExpressions.Regex.IsMatch(agent, WAPPattern))
            {
                ViewBag.CSS = "~/Content/WAP.css";
                return View("Search-WAP");
            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(agent, smartPhonePattern))
            {
                ViewBag.CSS = "~/Content/SmartPhone.css";
                return View("Search-SmartPhone");
            }
            else
            {
                ViewBag.CSS = "~/Content/Desktop.css";
                return View();
            }
        }
        public ActionResult header()
        {
            ClientUtil client = new ClientUtil(Request.UserAgent.ToLower());
            ViewBag.CSS = client.css;
            ViewBag.client = client.clientId;
            return PartialView();
        }

        public ActionResult footer()
        {
            ClientUtil client = new ClientUtil(Request.UserAgent.ToLower());
            ViewBag.CSS = client.css;
            ViewBag.client = client.clientId;
            return PartialView();
        }
    }
}
