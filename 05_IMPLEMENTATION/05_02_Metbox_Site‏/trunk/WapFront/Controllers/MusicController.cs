﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WapFront.Util;

namespace WapFront.Controllers
{
    public class MusicController : Controller
    {
        //
        // GET: /Music/

        public ActionResult Index(int? id)
        {
            ClientUtil client = new ClientUtil(Request.UserAgent.ToLower());
            ViewBag.CSS = client.css;
            ViewBag.CSSClass = "music";
            ViewBag.Title = "Âm nhạc";
            if (id.HasValue)
            {
                return View("Detail/Index" + client.client);
            }
            else
            {
                return View("Index" + client.client);
            }
            
        }

    }
}
