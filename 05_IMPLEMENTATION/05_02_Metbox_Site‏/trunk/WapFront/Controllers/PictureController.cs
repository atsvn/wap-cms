﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WapFront.Util;

namespace WapFront.Controllers
{
    public class PictureController : Controller
    {
        //
        // GET: /Picture/

        public ActionResult Index()
        {
            ClientUtil client = new ClientUtil(Request.UserAgent.ToLower());
            ViewBag.CSS = client.css;
            ViewBag.CSSClass = "image";
            ViewBag.Title = "Ảnh";
            return View("Index" + client.client);
        }

    }
}
