﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WapFront.Util;

namespace WapFront.Controllers
{
    public class VideoController : Controller
    {
        //
        // GET: /Video/

        public ActionResult Index(int? id)
        {
            ClientUtil client = new ClientUtil(Request.UserAgent.ToLower());
            ViewBag.CSS = client.css;
            ViewBag.Title = "Video";
            ViewBag.CSSClass = "video";
            if (id.HasValue)
            {
                return View("Detail/Index" + client.client);
            }
            else
            {
                return View("Index" + client.client);
            }
        }

    }
}
