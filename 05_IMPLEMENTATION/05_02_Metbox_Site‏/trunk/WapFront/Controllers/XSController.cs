﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace WapFront.Controllers
{
    public class XSController : Controller
    {
        //
        // GET: /XS/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(int zone)
        {
            ViewBag.Zone = zone;
            return View();
        }

        public ActionResult Register(int provinceid)
        {
            
            return View();
        }


        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }

    }
}
