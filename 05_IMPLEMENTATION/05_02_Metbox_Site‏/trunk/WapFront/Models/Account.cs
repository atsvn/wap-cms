﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace WapFront.Models
{
    public class Account
    {
        public int Id { get; set; }

        [Display(Name = "Created at")]
        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Display(Name = "Last modified")]
        [Required]
        public DateTime LastModifiedDateTime { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string Email { get; set; }

        public int Status { get; set; }
    }

    public class AccountDBContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}