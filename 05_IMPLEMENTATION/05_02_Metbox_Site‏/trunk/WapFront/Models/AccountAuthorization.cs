﻿using System.Web;
using System.Web.Mvc;

namespace WapFront.Models
{
    public class AccountAuthorization : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            HttpSessionStateBase session = httpContext.Session;
            return session["accountId"] != null;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            UrlHelper urlHelper = new UrlHelper(filterContext.RequestContext);
            filterContext.Result = new RedirectResult(urlHelper.Action("Signin", "Account"));
        }
    }
}