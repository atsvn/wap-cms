﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WapFront.Models
{
    public class Category
    {
        public int Id { get; set; }

        [Display(Name = "Created at")]
        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Display(Name = "Last modified")]
        [Required]
        public DateTime LastModifiedDateTime { get; set; }

        [Display(Name = "Category")]
        [Required]
        public string Name { get; set; }

        public int Status { get; set; }
    }
}