﻿using System.ComponentModel.DataAnnotations;

namespace WapFront.Models
{
    public class ChangePassword
    {
        [Display(Name = "Current password")]
        [Required]
        public string CurrentPassword { get; set; }

        [Display(Name = "New password")]
        [Required]
        public string NewPassword { get; set; }

        [Display(Name = "Confirm password")]
        [Required]
        public string ConfirmPassword { get; set; }
    }
}