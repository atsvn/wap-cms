﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.Mvc;

namespace WapFront.Models
{
    public class Game
    {
        private string[] sizeTypes = {" bytes", " KB", " MB", " GB"};

        public int Id { get; set; }

        [Display(Name = "Created at")]
        [Required]
        public DateTime CreatedAt { get; set; }

        [Display(Name = "Last modified")]
        [Required]
        public DateTime LastModified { get; set; }

        [Required]
        public string Name { get; set; }

        [AllowHtml]
        [Required]
        public string Description { get; set; }

        [Display(Name = "Image")]
        public string ImageURL { get; set; }

        public string Seller { get; set; }

        public int View { get; set; }

        public int Download { get; set; }

        public int Status { get; set; }

        [Display(Name = "Android Game (.apk)")]
        public string AndroidAppURL { get; set; }

        [Display(Name = "iOS Game (.ipa)")]
        public string iOSAppURL { get; set; }

        [Display(Name = "Java Game (.jar)")]
        public string JavaAppURL { get; set; }

        public float AndroidAppSize { get; set; }

        [NotMapped]
        public string AndroidAppSizeString
        {
            get {
                return AppSizeString(AndroidAppSize);
            }
        }

        public float iOSAppSize { get; set; }

        [NotMapped]
        public string iOSAppSizeString
        {
            get {
                return AppSizeString(iOSAppSize);
            }
        }

        public float JavaAppSize { get; set; }

        [NotMapped]
        public string JavaAppSizeString
        {
            get {
                return AppSizeString(iOSAppSize);
            }
        }

        [Display(Name = "Category")]
        public virtual ICollection<GameMapping> GameMappings { get; set; }

        private string AppSizeString(float size)
        {
            int i = 0;
            while (size > 1000)
            {
                i++;
                size = size / 1024;
            }

            return "(" + size.ToString("0.00") + sizeTypes[i] + ")";
        }
    }

    public class GameCategory
    {
        public int Id { get; set; }

        [Display(Name = "Created at")]
        [Required]
        public DateTime CreatedAt { get; set; }

        [Display(Name = "Last modified")]
        [Required]
        public DateTime LastModified { get; set; }

        [Required]
        public string Name { get; set; }

        public int Status { get; set; }
    }

    public class GameMapping
    {
        public int Id { get; set; }

        [Display(Name = "Created at")]
        [Required]
        public DateTime CreatedAt { get; set; }

        [Display(Name = "Last modified")]
        [Required]
        public DateTime LastModified { get; set; }

        [Column("CategoryId")]
        public int GameCategoryId { get; set; }

        public int GameId { get; set; }

        public int Status { get; set; }

        public virtual Game Game { get; set; }

        public virtual GameCategory GameCategory { get; set; }
    }

    public class GameDBContext : DbContext
    {
        public DbSet<Game> Games { get; set; }
        public DbSet<GameCategory> GameCategories { get; set; }
        public DbSet<GameMapping> GameMappings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}