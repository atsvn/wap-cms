﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace WapFront.Models
{
    public class Media
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Display(Name = "Created at")]
        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Display(Name = "Last modified")]
        [Required]
        public DateTime LastModifiedDateTime { get; set; }

        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        [Display(Name = "Long description")]
        [Required]
        public string LongDescription { get; set; }

        [Display(Name = "Short description")]
        [Required]
        public string ShortDescription { get; set; }

        public string Duration { get; set; }

        [Display(Name = "Thumb image")]
        public string ThumbImage { get; set; }

        public int View { get; set; }

        public int Status { get; set; }

        [NotMapped]
        public int zz { get; set; }

        public Category Category { get; set; }
    }

    public class MediaDBContext : DbContext
    {
        public DbSet<Media> Media { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Source> Sources { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}