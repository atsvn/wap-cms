﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace WapFront.Models
{
    public class Music
    {
        private string[] sizeType = { " bytes", " KB", " MB", " GB" };

        public int Id { get; set; }

        [Display(Name = "Created at")]
        [Required]
        public DateTime CreatedAt { get; set; }

        [Display(Name = "Last modified")]
        [Required]
        public DateTime LastModified { get; set; }

        [Required]
        public string Name { get; set; }

        [Display(Name = "Short description")]
        [Required]
        [StringLength(50)]
        public string ShortDescription { get; set; }

        [Display(Name = "Long description")]
        [Required]
        public string LongDescription { get; set; }

        public string Album { get; set; }

        public string Artist { get; set; }

        public int Year { get; set; }

        public string Duration { get; set; }

        public int View { get; set; }

        public int Download { get; set; }

        public int Status { get; set; }

        [Display(Name = "Image")]
        public string ImageURL { get; set; }

        [Display(Name = "Source URL")]
        public string SourceURL { get; set; }

        public float Size { get; set; }

        [NotMapped]
        public string SizeString
        {
            get
            {
                int i = 0;
                float size = Size;
                while (size > 1000)
                {
                    i++;
                    size = size / 1024;
                }

                return "(" + size.ToString("0.00") + sizeType[i] + ")";
            }
        }
    }   

    public class MusicDBContext : DbContext
    {
        public DbSet<Music> Musics { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}