﻿using System.ComponentModel.DataAnnotations;
namespace WapFront.Models
{
    public class Source
    {
        public int Id { get; set; }

        [Display(Name = "Media")]
        public int MediaId { get; set; }

        [Range(1, 4)]
        public int Type { get; set; }

        public float Size { get; set; }

        [Required]
        public string URL { get; set; }

        public Media Media { get; set; }
    }
}