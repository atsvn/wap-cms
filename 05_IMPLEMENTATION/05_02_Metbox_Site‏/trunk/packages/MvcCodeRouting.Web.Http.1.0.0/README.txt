MvcCodeRouting
=============================================================================== 

MvcCodeRouting for ASP.NET MVC analyzes your controllers and actions and 
automatically creates the best possible routes for them.

Organize your controllers and views using namespaces (no more areas) that 
can go as deep as you want.

Includes support for hierarchical (a.k.a. RESTful) and also user-defined 
custom routes (using an attribute).

It also supports embedded views (as assembly resources) that can be 
individually overridden by file views.

MvcCodeRouting is a single unified solution that provides namespace-aware 
automatic route creation, URL generation and views location.

Licensing
=============================================================================== 

This software is licensed under the terms you may find in the file
named "LICENSE.txt" of this distribution.

Getting Started
=============================================================================== 

Import the MvcCodeRouting namespace and call the MapCodeRoutes extension 
method:

    void Register(HttpConfiguration config) {

        config.MapCodeRoutes(
            rootController: typeof(Controllers.HomeController)
        );
    }

For ASP.NET hosting install the MvcCodeRouting.Web.Http.WebHost package.

Please refer to the website for more information
  
  http://mvccoderouting.codeplex.com/

Please help us make MvcCodeRouting better - we appreciate any feedback
you may have.

$ Donate
=============================================================================== 

If you would like to show your appreciation for MvcCodeRouting, please 
consider making a small donation

  http://mvccoderouting.codeplex.com/wikipage?title=Donate

Changes
=============================================================================== 
1.0.0 - Requires MvcCodeRouting v1.2
      - Moved WebHost to separate assembly/package

0.10.0 - Implemented BindRouteProperties
       - Fixed #1152: Both traditional and verb-based routing cannot be used in 
         the same ApiController
       - Using Guid as route name in self host

0.9.3 - Requires MvcCodeRouting v1.1
      - Fix: Web.Http.FromRoute attribute does not bind when using a custom name
      - Changed Web.Http MapCodeRoutes to extend HttpConfiguration instead of 
        HttpRouteCollection. Calling CodeRoutingSettings.HttpConfiguration() is 
        no longer needed, that method is now internal.

0.9.2 - No binary changes, updated MvcCodeRouting dependency version range.

0.9.1 - First CTP release