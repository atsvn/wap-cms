MvcCodeRouting
=============================================================================== 

MvcCodeRouting for ASP.NET MVC analyzes your controllers and actions and 
automatically creates the best possible routes for them.

Organize your controllers and views using namespaces (no more areas) that 
can go as deep as you want.

Includes support for hierarchical (a.k.a. RESTful) and also user-defined 
custom routes (using an attribute).

It also supports embedded views (as assembly resources) that can be 
individually overridden by file views.

MvcCodeRouting is a single unified solution that provides namespace-aware 
automatic route creation, URL generation and views location.

Licensing
=============================================================================== 

This software is licensed under the terms you may find in the file
named "LICENSE.txt" of this distribution.

Getting Started
=============================================================================== 

Import the MvcCodeRouting namespace and call the MapCodeRoutes extension 
method:

    void RegisterRoutes(RouteCollection routes) {

        routes.MapCodeRoutes(
            rootController: typeof(Controllers.HomeController)
        );
    }

Please refer to the website for more information
  
  http://mvccoderouting.codeplex.com/

Please help us make MvcCodeRouting better - we appreciate any feedback
you may have.

$ Donate
=============================================================================== 

If you would like to show your appreciation for MvcCodeRouting, please 
consider making a small donation

  http://mvccoderouting.codeplex.com/wikipage?title=Donate

Changes
=============================================================================== 
1.0.0 - First release